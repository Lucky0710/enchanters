﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Utils
{
    class DictUtils
    {
        /// <summary>
        /// Get the value in a dictionnary for a key or return the specified value
        /// </summary>
        /// <param name="dict">The dictionnary</param>
        /// <param name="key">The key</param>
        /// <param name="def">The default value</param>
        /// <returns>The value corresponding to the key if it exists, def otherwise</returns>
        public static T DictGetOrDefault<T>(Dictionary<string, object> dict, string key, T def)
        {
            return dict.ContainsKey(key) ? (T)dict[key] : def;
        }
    }
}
