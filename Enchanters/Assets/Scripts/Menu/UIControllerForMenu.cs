using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIControllerForMenu : MonoBehaviour
{
    public static UIControllerForMenu Instance => _instance;
    private static UIControllerForMenu _instance;

    private void Awake()
    {
        _instance = this;
    }

    public List<BluredBG> BluredBgs = new List<BluredBG>();

    public MainMenuScreen MainMenuScreen;

    public Image DarkFade;
    
    #region Screen Management
    
    private void TransitionScreen(MonoBehaviour screen, bool withFade = true)
    {
        if (withFade)
        {
            FadeDark(3f);
            DOVirtual.DelayedCall(1.5f, () =>
            {
                ActiveOneScreen(screen.gameObject);
            });
        }
        else
        {
            ActiveOneScreen(screen.gameObject);
        }
    }
    
    private void ActiveOneScreen(GameObject screen)
    {
        MainMenuScreen.gameObject.SetActive(screen == MainMenuScreen.gameObject);
    }

    public void InitMenuScreen(bool withFade = true)
    {
        TransitionScreen(MainMenuScreen, withFade);
    }

    private void FadeDark(float duration)
    {
        DarkFade.DOKill();
        DarkFade.gameObject.SetActive(true);
        DarkFade.raycastTarget = true;
        DarkFade.DOFade(1f, duration * 0.33f).From(0f).OnComplete(() =>
        {
            DOVirtual.DelayedCall(duration * 0.33f, () =>
            {
                DarkFade.raycastTarget = false;
                DarkFade.DOFade(0f, duration * 0.33f).OnComplete(() => { DarkFade.gameObject.SetActive(false); });
            });
        });
    }
    
    #endregion
}
