using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using DG.Tweening;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    public static MenuController Instance => _instance;
    private static MenuController _instance;

    private void Awake()
    {
        _instance = this;
    }

    public LocalSave LocalSave;
    public User CurrentUser;

    private void Start()
    {
        LoadData();
        UIControllerForMenu.Instance.InitMenuScreen();
        
        if (LocalSave.Users.Count == 0)
        {
            UIControllerForMenu.Instance.MainMenuScreen.AccountsPopup.Open();
            UIControllerForMenu.Instance.MainMenuScreen.Username.text = "";
        }
        else
        {
            if (LocalSave.LastUserId >= LocalSave.Users.Count)
                CurrentUser = LocalSave.Users[0];
            else
                CurrentUser = LocalSave.Users[LocalSave.LastUserId];
            UIControllerForMenu.Instance.MainMenuScreen.Username.text = CurrentUser.Username;
        }
        
        LobbyManager.Instance.ConnectToPhoton();
    }

    #region Application Lifecycle
    
    public void OnApplicationQuit()
    {
    }

    #endregion

    #region Accounts Management

    public void SwitchAccount(User user)
    {
        CurrentUser = user;
        for (int i = 0; i < LocalSave.Users.Count; i++)
        {
            if (LocalSave.Users[i] == CurrentUser)
                LocalSave.LastUserId = i;
        }
        
        UIControllerForMenu.Instance.MainMenuScreen.Username.text = CurrentUser.Username;
    }

    public void CreateAccount(string username)
    {
        User newUser = new User() {Username = username};
        LocalSave.Users.Add(newUser);
        SaveData();
        SwitchAccount(newUser);
    }

    public void DeleteAccount(User user)
    {
        LocalSave.Users.Remove(user);
        UIControllerForMenu.Instance.MainMenuScreen.AccountsPopup.Init();
        SaveData();
    }


    #endregion

    #region Save Management

    public void SaveData()
    {
        Debug.Log("Saving data");
        XmlSerializer serializer = new XmlSerializer(typeof(LocalSave));

        using (FileStream stream = new FileStream(Application.persistentDataPath + "/LocalSave.xml", FileMode.Create))
        {
            serializer.Serialize(stream, LocalSave);
        }

        Debug.Log("Game Saved");
    }

    public void LoadData()
    {
        bool succeded = false;
        
        if (File.Exists(Application.persistentDataPath + "/LocalSave.xml"))
        {
            XmlSerializer serializer = new XmlSerializer(typeof(LocalSave));
 
            using (FileStream stream = new FileStream(Application.persistentDataPath + "/LocalSave.xml", FileMode.Open))
            {
                try
                {
                    LocalSave = (LocalSave) serializer.Deserialize(stream);
                    succeded = true;
                }
                catch
                {
                    succeded = false;
                }
            }
        }

        if (!succeded)
        {
            Debug.Log("No game saved!");
            LocalSave = new LocalSave();
            LocalSave.Users = new List<User>();
            SaveData();
        }
    }
    
    #endregion
}
