﻿using System;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LobbyManager : MonoBehaviourPunCallbacks
{
    public static LobbyManager Instance => _instance;
    private static LobbyManager _instance;

    private void Awake()
    {
        _instance = this;
    }

    public GameObject MainPanel;
    public GameObject WaitingPanel;
    public GameObject LoadingPanel;
    public Text LoadingText;
    public Text LobbyKeyDisplayer;
    public InputField LobbyKeyInputField;

    private void Start()
    {
        LobbyKeyInputField.onValidateInput += delegate(string input, int index, char added)
        {
            return input.Length < 6 ? char.ToUpper(added) : '\0';
        };
        
        MainPanel.SetActive(false);
        WaitingPanel.SetActive(false);
        
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    public void ConnectToPhoton()
    {
        Debug.Log("Connecting to Photon Network");
        PhotonNetwork.GameVersion = "1";
        PhotonNetwork.ConnectUsingSettings();
        LoadingPanel.SetActive(true);
        LoadingText.text = "Connecting to Photon...";
    }

    public override void OnConnected()
    {
        base.OnConnected();
        Debug.Log("Connected to Photon !");
        LoadingPanel.SetActive(false);
        MainPanel.SetActive(true);
    }

    public void OnCreateLobbyClick()
    {
        if (PhotonNetwork.IsConnected)
        {
            MainPanel.SetActive(false);
            LoadingPanel.SetActive(true);
            LoadingText.text = "Creating room...";
            PhotonNetwork.LocalPlayer.NickName = MenuController.Instance.CurrentUser.Username;
            Debug.Log("PhotonNetwork.IsConnected! | Trying to create new room");
            PhotonNetwork.CreateRoom(NewRoomName(), new RoomOptions {MaxPlayers = 2});
        }
    }

    public void OnJoinLobbyClick()
    {
        if (PhotonNetwork.IsConnected)
        {
            MainPanel.SetActive(false);
            LoadingPanel.SetActive(true);
            LoadingText.text = "Joining room...";
            PhotonNetwork.LocalPlayer.NickName = MenuController.Instance.CurrentUser.Username;
            var lobbyKey = LobbyKeyInputField.text;
            Debug.Log("PhotonNetwork.IsConnected! | Trying to join room " + lobbyKey);
            PhotonNetwork.JoinRoom(lobbyKey);
        }
    }
    
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        LoadingPanel.SetActive(false);
        if (PhotonNetwork.IsMasterClient)
        {
            WaitingPanel.SetActive(true);
            LobbyKeyDisplayer.text = PhotonNetwork.CurrentRoom.Name;
            Debug.Log("Joined room as master");
        }
        else
        {
            Debug.Log("Joined room as opponent");
        }
    }

    public void OnCancelClicked()
    {
        PhotonNetwork.LeaveRoom();
        WaitingPanel.SetActive(false);
        LoadingPanel.SetActive(true);
        LoadingText.text = "Leaving room...";
    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        MainPanel.SetActive(true);
        LoadingPanel.SetActive(false);
        Debug.Log("Left room");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        Debug.Log("Player : " + newPlayer.NickName + " entered Room");
        if (PhotonNetwork.IsMasterClient && PhotonNetwork.CurrentRoom.PlayerCount == 2)
        {
            PhotonNetwork.LoadLevel("MatchScene");
        }
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);
        Debug.Log("Create room failed");
        PhotonNetwork.CreateRoom(NewRoomName(), new RoomOptions { MaxPlayers = 2 });
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        base.OnJoinRoomFailed(returnCode, message);
        Debug.Log("Join room failed");
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.LogError("Disconnected. Please check your Internet connection.");
    }

    public string NewRoomName()
    {
        return Convert.ToBase64String(Guid.NewGuid().ToByteArray())
            .Replace("/", "")
            .Replace("+", "")
            .Replace("0", "")
            .Replace("O", "")
            .Replace("o", "")
            .Substring(0, 6)
            .ToUpper();
    }
}