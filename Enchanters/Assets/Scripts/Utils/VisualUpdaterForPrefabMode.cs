using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class VisualUpdaterForPrefabMode : MonoBehaviour
{
    public Card Prefab;
    private void Update()
    {
        if (Prefab)
        {
            Prefab.UpdateCostVisuals();
            Prefab.UpdateIndicatorVisuals();
        }
    }
}
