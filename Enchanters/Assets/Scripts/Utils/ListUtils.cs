using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ListUtils
{
    public static T RandomItem<T>(this IList<T> ts)
    {
        if (ts.Count == 0) return default;
        if (ts.Count == 1) return ts[0];

        return ts[Random.Range(0, ts.Count)];
    }
}

public static class IListExtensions {
    /// <summary>
    /// Shuffles the element order of the specified list.
    /// </summary>
    public static void Shuffle<T>(this IList<T> ts) {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i) {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }
}
