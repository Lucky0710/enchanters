using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using DG.Tweening;
using ExitGames.Client.Photon.StructWrapping;
using Photon.Pun;
using Photon.Realtime;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

public class MatchController : MonoBehaviourPunCallbacks
{
    public static MatchController Instance => _instance;
    private static MatchController _instance;

    private void Awake()
    {
        _instance = this;
    }

    public bool Online;
    public PlayerController OfflinePlayerPrefab;

    public int MaxCardsInHand;
    public int MarketPoolSize;
    
    public int BaseLifePoints;
    public int BaseMana;
    public int BaseManaGrowth;
    public int BaseCoins;
    public int BaseCoinsGrowth;

    public float DiscardedCardThickness;

    public List<Card> AllCards;
    public List<Card> OfflineStartHand;
    public List<SpellSlot> SpellSlotsForLocalPlayer;
    public List<SpellSlot> SpellSlotsForOpponent;
    public Transform CleanRoot;
    public PhotonView PhotonView;

    [HideInInspector] public List<Card> MarketPool = new List<Card>();
    [HideInInspector] public List<Card> Market = new List<Card>();
    [HideInInspector] public List<Card> MarketDiscardPile = new List<Card>();
    
    private List<Card> LocalMulligan = new List<Card>();
    private List<Card> OpponentMulligan = new List<Card>();

    [HideInInspector] public PlayerController Opponent;
    [HideInInspector] public PlayerController LocalPlayer;

    private bool _localPlayerTurn;
    private bool _gameOver = false;
    private bool _mulligan;
    private bool _localMulliganValidated;
    private bool _opponentMulliganValidated;

    #region Initialisation

    private void Start()
    {
        if (Online)
        {
            if (!PhotonNetwork.IsConnected || !PhotonNetwork.InRoom)
            {
                SceneManager.LoadScene(0);
                return;
            }

            CreateLocalPlayer();
        }
        else
            InitOfflineGame();
    }

    private void InitOfflineGame()
    {
        LocalPlayer = Instantiate(OfflinePlayerPrefab);
        Opponent = Instantiate(OfflinePlayerPrefab);
        LocalPlayer.Username = "Local";
        Opponent.Username = "Opponent";
        LocalPlayer.InitForMatch(BaseLifePoints, BaseMana, BaseCoins, BaseManaGrowth, BaseCoinsGrowth);
        Opponent.InitForMatch(BaseLifePoints, BaseMana, BaseCoins, BaseManaGrowth, BaseCoinsGrowth);
        UIControllerForGame.Instance.SetPlayersUI(LocalPlayer, Opponent);

        _localPlayerTurn = true;

        for (int i = 0; i < MarketPoolSize; i++)
        {
            RpcCreateCardToMarketPool(AllCards.RandomItem().CodeName, Guid.NewGuid().ToString());
        }

        foreach (var card in OfflineStartHand)
        {
            RpcCreateCardToMarketPool(card.CodeName, Guid.NewGuid().ToString());
            RpcPlayerDrawCard(LocalPlayer.Username, MarketPool[MarketPool.Count - 1].Id.ToString());
        }

        for (int i = 0; i < 5; i++)
        {
            RpcPlayerDrawCard(Opponent.Username, MarketPool.RandomItem().Id.ToString());
        }

        RpcInitLocalMarket();
        LocalPlayer.GainStatsBetweenTurns();
        UIControllerForGame.Instance.UpdateUI();
    }

    private void CreateLocalPlayer()
    {
        LocalPlayer = PhotonNetwork.Instantiate("PlayerController", new Vector3(0f, 0f, 0f),
            Quaternion.identity, 0).GetComponent<PlayerController>();
        LocalPlayer.InitForMatch(BaseLifePoints, BaseMana, BaseCoins, BaseManaGrowth, BaseCoinsGrowth);
    }

    public void InitOpponent(PlayerController opponent)
    {
        Opponent = opponent;
        Opponent.InitForMatch(BaseLifePoints, BaseMana, BaseCoins, BaseManaGrowth, BaseCoinsGrowth);
        if (LocalPlayer)
        {
            SetPlayersUsername();
            InitMatch();
        }
    }

    private void SetPlayersUsername()
    {
        foreach (var player in PhotonNetwork.CurrentRoom.Players)
        {
            if (player.Value.IsMasterClient)
            {
                if (PhotonNetwork.IsMasterClient)
                    LocalPlayer.Username = player.Value.NickName;

                else
                    Opponent.Username = player.Value.NickName;
            }
            else
            {
                if (PhotonNetwork.IsMasterClient)
                    Opponent.Username = player.Value.NickName;

                else
                    LocalPlayer.Username = player.Value.NickName;
            }
        }
    }

    #endregion

    #region Match Flow

    private void InitMatch()
    {
        _gameOver = false;
        _mulligan = true;
        UIControllerForGame.Instance.SetPlayersUI(LocalPlayer, Opponent);

        if (PhotonNetwork.IsMasterClient)
        {
            InitMarket();
            InitPlayerMulligans();            
            PhotonView.RPC("RpcSyncRandom", RpcTarget.All, Random.Range(Int32.MinValue, Int32.MaxValue));
        }
        
        UIControllerForGame.Instance.InitMulliganScreen();
        UIControllerForGame.Instance.UpdateUI();
    }

    private void InitMarket()
    {
        for (int i = 0; i < MarketPoolSize; i++)
        {
            PhotonView.RPC("RpcCreateCardToMarketPool", RpcTarget.All, AllCards.RandomItem().CodeName,
                Guid.NewGuid().ToString());
        }
    }

    private void InitPlayerMulligans()
    {
        _localMulliganValidated = false;
        _opponentMulliganValidated = false;
        
        List<Card> marketPoolCopy = new List<Card>();
        marketPoolCopy.AddRange(MarketPool);
        
        for (int i = 0; i < 8; i++)
        {
            Card chosen = marketPoolCopy.RandomItem();
            PhotonView.RPC("RpcPlayerDrawMulliganCard", RpcTarget.All, LocalPlayer.Username,
                chosen.Id.ToString());
            marketPoolCopy.Remove(chosen);
        }

        for (int i = 0; i < 8; i++)
        {
            Card chosen = marketPoolCopy.RandomItem();
            PhotonView.RPC("RpcPlayerDrawMulliganCard", RpcTarget.All, Opponent.Username,
                chosen.Id.ToString());
            marketPoolCopy.Remove(chosen);
        }
    }

    public void ValidateMulligan()
    {
        int id = 0;
        foreach (var card in LocalMulligan)
        {
            if (card.SelectedInMulligan)
            {
                PhotonView.RPC("RpcOpponentValidatedMulligan", RpcTarget.Others, LocalMulligan.IndexOf(card));
                AnimationManager.Instance.AppendAnim(new AnimationTemplate(AnimationManager.Instance.ChooseMulliganCard, new List<Card>(){card}, 0.7f, 0f, false, new List<int>(){id}));
                id++;
            }
            else
            {
                card.gameObject.SetActive(false);
            }
        }
        
        _localMulliganValidated = true;
        if (PhotonNetwork.IsMasterClient)
        {
            if (_opponentMulliganValidated)
                PhotonView.RPC("RpcStartMatch", RpcTarget.All);
        }
        else
        {
            PhotonView.RPC("RpcValidateClientMulligan", RpcTarget.MasterClient);
        }
    }

    public void IncrementMarket()
    {
        Card drawn = MarketPool[MarketPool.Count - 1];
        MarketPool.Remove(drawn);
        Market.Insert(0, drawn);
        AnimationManager.Instance.AppendAnim(new AnimationTemplate(AnimationManager.Instance.PopNewMarketCard, new List<Card>(){drawn}, 1f, 0f, false));

        if (Market.Count > 5)
        {
            Card discarded = Market[Market.Count - 1];
            Market.Remove(discarded);
            MarketDiscardPile.Insert(0, discarded);
            AnimationManager.Instance.AppendAnim(new AnimationTemplate(AnimationManager.Instance.DisapearLastMarketCard, new List<Card>(){discarded}, 1f, 0f, false));
        }

        for (int i = 1; i < Market.Count; i++)
        {
            AnimationManager.Instance.AppendAnim(new AnimationTemplate(
                AnimationManager.Instance.SlideToMarketSlot, new List<Card>{Market[i]}, 1f, 0f, true, new List<int>{i}));
        }
        
        UIControllerForGame.Instance.UpdateUI();
    }
    
    public void DecrementMarket()
    {
        Card drawn = null;
        if (MarketDiscardPile.Count > 0)
            drawn = MarketDiscardPile[0];

        if (drawn)
        {
            MarketDiscardPile.Remove(drawn);        
            Market.Add(drawn);
            AnimationManager.Instance.AppendAnim(new AnimationTemplate(AnimationManager.Instance.PopNewMarketCard, new List<Card>(){drawn}, 1f, 0f, true));
        }

        if (Market.Count > 0)
        {
            Card discarded = Market[0];
            Market.Remove(discarded);
            MarketPool.Add(discarded);
            AnimationManager.Instance.AppendAnim(new AnimationTemplate(AnimationManager.Instance.DisapearLastMarketCard, new List<Card>(){discarded}, 1f, 0f, true));
        }

        for (int i = 0; i < Market.Count - 1; i++)
        {
            AnimationManager.Instance.AppendAnim(new AnimationTemplate(
                AnimationManager.Instance.SlideToMarketSlot, new List<Card>{Market[i]}, 1f, 0f, true, new List<int>{i}));
        }
        
        UIControllerForGame.Instance.UpdateUI();
    }

    public void CheckForGameOver()
    {
        if (_gameOver)
            return;
        
        if (LocalPlayer.LifePoints <= 0 || LocalPlayer.Sanity <= -30 || Opponent.Sanity >= 30)
        {
            PlayerWinGame(Opponent.Username);
        }

        if (Opponent.LifePoints <= 0 || Opponent.Sanity <= -30 || LocalPlayer.Sanity >= 30)
        {
            PlayerWinGame(LocalPlayer.Username);
        }
    }
    
    [PunRPC]
    public void PlayerWinGame(string playerId)
    {
        _gameOver = true;
        UIControllerForGame.Instance.InitEndScreen(playerId == LocalPlayer.Username, playerId);
    }

    public void EndLocalPlayerTurn()
    {
        if (!_localPlayerTurn)
            return;

        if (!Online)
        {
            IncrementMarket();
            return;
        }
        
        _localPlayerTurn = false;
        LocalPlayer.ResetTurnEnchants();
        UIControllerForGame.Instance.UpdateUI();
        PhotonView.RPC("RpcStartPlayerTurn", RpcTarget.All, Opponent.Username);
    }

    public bool TryPlayCard(Card card)
    {
        if (!_localPlayerTurn || !CanPlayCard(LocalPlayer, card))
            return false;

        if (Online)
            PhotonView.RPC("RpcPlayerPlayCard", RpcTarget.All, LocalPlayer.Username, card.Id.ToString());
        else
            RpcPlayerPlayCard(LocalPlayer.Username, card.Id.ToString());
        return true;
    }
    
    public bool TryBuyCard(Card card)
    {
        if (!_localPlayerTurn || !CanBuyCard(LocalPlayer, card))
            return false;

        if (Online)
            PhotonView.RPC("RpcPlayerBuyCard", RpcTarget.All, LocalPlayer.Username, card.Id.ToString());
        else
            RpcPlayerBuyCard(LocalPlayer.Username, card.Id.ToString());
        return true;
    }
    
    public bool TryCastSpell(SpellSlot slot)
    {
        if (!_localPlayerTurn || slot.Card.Type != CardType.Spell || !CanCastSpell(LocalPlayer, slot.Card))
            return false;

        if (Online)
            PhotonView.RPC("RpcPlayerCastSpell", RpcTarget.All, LocalPlayer.Username, SpellSlotsForLocalPlayer.IndexOf(slot));
        else
            RpcPlayerCastSpell(LocalPlayer.Username, SpellSlotsForLocalPlayer.IndexOf(slot));
        return true;
    }

    public bool TryFreeSlot(SpellSlot slot)
    {
        if (!_localPlayerTurn || !slot.Card)
            return false;
        
        if (Online)
            PhotonView.RPC("RpcPlayerFreeSlot", RpcTarget.All, LocalPlayer.Username, SpellSlotsForLocalPlayer.IndexOf(slot));
        else
            RpcPlayerFreeSlot(LocalPlayer.Username, SpellSlotsForLocalPlayer.IndexOf(slot));
        return true;
    }

    #endregion
    
    #region Networking Game Actions
    
    [PunRPC]
    public void RpcSyncRandom(int seed)
    {
        Random.InitState(seed);
    }
    
    [PunRPC]
    public void RpcCreateCardToMarketPool(string codeName, string id)
    {
        Card prefab = GetCardPrefabFromCodename(codeName);
        Card created = Instantiate(prefab, CleanRoot);
        created.Id = Guid.Parse(id);
        MarketPool.Add(created);
        Debug.Log("Created card with id : " + id);
    }
    
    [PunRPC]
    public void RpcInitLocalMarket()
    {
        for (int i = 0; i < 5; i++)
        {
            IncrementMarket();
        }
    }
    
    
    [PunRPC]
    public void RpcPlayerDrawMulliganCard(string playerId, string cardId)
    {
        Card drawn = GetCardFromMarketPool(Guid.Parse(cardId));
        MarketPool.Remove(drawn);
        
        if (IsLocalPlayer(playerId))
        {
            LocalMulligan.Add(drawn);
            AnimationManager.Instance.AppendAnim(new AnimationTemplate(AnimationManager.Instance.PopMulliganCard, 
                new List<Card>(){drawn}, 1f, 0.1f, true, new List<int>(){LocalMulligan.Count - 1}));
        }
        else
        {
            OpponentMulligan.Add(drawn);
        }
    }

    [PunRPC]
    public void RpcOpponentValidatedMulligan(int id)
    {
        OpponentMulligan[id].SelectedInMulligan = true;
    }
    
    [PunRPC]
    public void RpcValidateClientMulligan()
    {
        if (_localMulliganValidated)
            PhotonView.RPC("RpcStartMatch", RpcTarget.All);
        else
        {
            _opponentMulliganValidated = true;
        }
        
        UIControllerForGame.Instance.UpdateUI();
    }
    
    [PunRPC]
    public void RpcStartMatch()
    {
        _mulligan = false;
        
        DOVirtual.DelayedCall(2f, () =>
        {
            if (PhotonNetwork.IsMasterClient)
            {
                _localPlayerTurn = true;
                PhotonView.RPC("RpcInitLocalMarket", RpcTarget.All);
                LocalPlayer.GainStatsBetweenTurns();
            }
            else
            {
                _localPlayerTurn = false;
                Opponent.GainStatsBetweenTurns();
            }

            foreach (var card in LocalMulligan)
            {
                if (card.SelectedInMulligan)
                    LocalPlayer.Hand.Add(card);
            }
        
            foreach (var card in OpponentMulligan)
            {
                if (card.SelectedInMulligan)
                    Opponent.Hand.Add(card);
            }
        
            UIControllerForGame.Instance.RearangeHand(Opponent);
            UIControllerForGame.Instance.RearangeHand(LocalPlayer);
            UIControllerForGame.Instance.DismissMulliganScreen();
            UIControllerForGame.Instance.UpdateUI();
        });
    }

    [PunRPC]
    public void RpcStartPlayerTurn(string playerId)
    {
        if (IsLocalPlayer(playerId))
        {
            Opponent.ResetTurnEnchants();
            _localPlayerTurn = true;
            LocalPlayer.GainStatsBetweenTurns();
            TriggerPassiveAbilities(TriggerKeyword.TurnStart, PlayerTarget.LocalPlayer);
        }
        else
        {
            _localPlayerTurn = false;
            Opponent.GainStatsBetweenTurns();
            TriggerPassiveAbilities(TriggerKeyword.TurnStart, PlayerTarget.Opponent);
        }
        
        IncrementMarket();
        ResetSlots(!_localPlayerTurn);
        UIControllerForGame.Instance.UpdateUI();
    }

    [PunRPC]
    public void RpcPlayerDrawCard(string playerId, string cardId)
    {
        Card drawn = GetCardFromMarketPool(Guid.Parse(cardId));
        MarketPool.Remove(drawn);
        
        if (IsLocalPlayer(playerId))
        {
            Debug.Log("Drawing card as LocalPlayer");
            LocalPlayer.Hand.Add(drawn);
            AnimationManager.Instance.AppendAnim(new AnimationTemplate(AnimationManager.Instance.DrawCard, 
                new List<Card>(){drawn}, 1f, 0.2f, true));
        }
        else
        {
            Debug.Log("Drawing card as Opponent");
            Opponent.Hand.Add(drawn);
            AnimationManager.Instance.AppendAnim(new AnimationTemplate(AnimationManager.Instance.DrawCard, 
                new List<Card>(){drawn}, 1f, 0.2f, false));
        }
        
        UIControllerForGame.Instance.UpdateUI();
    }


    [PunRPC]
    public void RpcPlayerPlayCard(string playerId, string cardId)
    {
        if (IsLocalPlayer(playerId))
        {
            foreach (var card in LocalPlayer.Hand)
            {
                if (card.Id == Guid.Parse(cardId))
                {
                    if (LocalPlayer.SpendMadnessInsteadOfMana)
                        IncreasePlayerSanity(LocalPlayer, -card.PlayCost);
                    else
                        IncreasePlayerMana(LocalPlayer, -card.PlayCost);
                    
                    LocalPlayer.Hand.Remove(card);
                    UIControllerForGame.Instance.RearangeHand(LocalPlayer);

                    bool learnedSpell = false;

                    if (card.Type == CardType.Parchment)
                    {
                        ExecuteCardAbilities(card, LocalPlayer);
                    }
                    else
                    {
                        SpellSlot freeSlot = GetFirstFreeSlot(true);
                        freeSlot.Init(card, true);
                        LocalPlayer.ActiveSlots.Add(freeSlot);
                        learnedSpell = true;
                    }
                    
                    LocalPlayer.DiscardPile.Add(card);
                    LocalPlayer.LastPlayedCard = card;
                    LocalPlayer.PlayedThisTurn.Add(card);
                    UIControllerForGame.Instance.PlacePlayedCardToSide(card);
                    DOVirtual.DelayedCall(1.2f, () => AnimationManager.Instance.AppendAnim(new AnimationTemplate(
                        AnimationManager.Instance.DiscardCard,
                        new List<Card>() {card}, 0.5f, 0.5f, true, null, ComputeDiscardPileHeight(true, card))));
                    
                    TriggerPassiveAbilities(TriggerKeyword.PlayedCard, PlayerTarget.LocalPlayer);
                    if (learnedSpell)
                        TriggerPassiveAbilities(TriggerKeyword.LearnedSpell, PlayerTarget.LocalPlayer);

                    if (LocalPlayer.Hand.Count == 0)
                    {
                        TriggerPassiveAbilities(TriggerKeyword.HandIsEmpty, PlayerTarget.LocalPlayer);
                    }

                    break;
                }
            }
        }
        else
        {
            foreach (var card in Opponent.Hand)
            {
                if (card.Id == Guid.Parse(cardId))
                {
                    if (Opponent.SpendMadnessInsteadOfMana)
                        IncreasePlayerSanity(Opponent, -card.PlayCost);
                    else
                        IncreasePlayerMana(Opponent, -card.PlayCost);
                    
                    Opponent.Hand.Remove(card);
                    UIControllerForGame.Instance.RearangeHand(Opponent);
                    
                    bool learnedSpell = false;

                    if (card.Type == CardType.Parchment)
                    {
                        ExecuteCardAbilities(card, Opponent);
                    }
                    else
                    {
                        SpellSlot freeSlot = GetFirstFreeSlot(false);
                        freeSlot.Init(card, false);
                        Opponent.ActiveSlots.Add(freeSlot);
                        learnedSpell = true;
                    }
                    
                    Opponent.DiscardPile.Add(card);
                    Opponent.LastPlayedCard = card;
                    Opponent.PlayedThisTurn.Add(card);

                    UIControllerForGame.Instance.PlacePlayedCardToSide(card);
                    DOVirtual.DelayedCall(0.5f, () => AnimationManager.Instance.AppendAnim(new AnimationTemplate(
                        AnimationManager.Instance.DiscardCard,
                        new List<Card>() {card}, 0.5f, 0.5f, false, null, ComputeDiscardPileHeight(false, card))));
                    
                    TriggerPassiveAbilities(TriggerKeyword.PlayedCard, PlayerTarget.Opponent);
                    if (learnedSpell)
                        TriggerPassiveAbilities(TriggerKeyword.LearnedSpell, PlayerTarget.Opponent);

                    if (Opponent.Hand.Count == 0)
                    {
                        TriggerPassiveAbilities(TriggerKeyword.HandIsEmpty, PlayerTarget.Opponent);
                    }

                    break;
                }
            }
        }
        
        UIControllerForGame.Instance.UpdateUI();
    }
    
    [PunRPC]
    public void RpcPlayerBuyCard(string playerId, string cardId)
    {
        foreach (var card in Market)
        {
            if (card.Id == Guid.Parse(cardId))
            {
                Market.Remove(card);
                
                if (IsLocalPlayer(playerId))
                {
                    IncreasePlayerCoins(LocalPlayer, -card.BuyCost);
                    LocalPlayer.Hand.Add(card);
                    LocalPlayer.LastBoughtCard = card;
                    TriggerPassiveAbilities(TriggerKeyword.BoughtCard, PlayerTarget.LocalPlayer);
                    UIControllerForGame.Instance.RearangeHand(LocalPlayer);
                }
                else
                {
                    IncreasePlayerCoins(Opponent, -card.BuyCost);
                    Opponent.Hand.Add(card);
                    Opponent.LastBoughtCard = card;
                    TriggerPassiveAbilities(TriggerKeyword.BoughtCard, PlayerTarget.Opponent);
                    UIControllerForGame.Instance.RearangeHand(Opponent);
                }

                IncrementMarket();

                break;
            }
        }
        
        UIControllerForGame.Instance.UpdateUI();
    }
    
    [PunRPC]
    public void RpcPlayerCastSpell(string playerId, int slotId)
    {
        if (IsLocalPlayer(playerId))
        {
            SpellSlot slot = SpellSlotsForLocalPlayer[slotId];
            slot.Use();
            ExecuteCardAbilities(slot.Card, LocalPlayer);
            LocalPlayer.LastCastSpell = slot.Card;
        }
        else
        {
            SpellSlot slot = SpellSlotsForOpponent[slotId];
            slot.Use();
            ExecuteCardAbilities(slot.Card, Opponent);
            Opponent.LastCastSpell = slot.Card;
        }
        
        UIControllerForGame.Instance.UpdateUI();
    }
    
    [PunRPC]
    public void RpcPlayerFreeSlot(string playerId, int slotId)
    {
        if (IsLocalPlayer(playerId))
        {
            LocalPlayer.ActiveSlots.Remove(SpellSlotsForLocalPlayer[slotId]);
            SpellSlotsForLocalPlayer[slotId].Free();
        }
        else
        {
            Opponent.ActiveSlots.Remove(SpellSlotsForOpponent[slotId]);
            SpellSlotsForOpponent[slotId].Free();
        }
        
        UIControllerForGame.Instance.UpdateUI();
    }

    #endregion

    #region Abilities

    public void TriggerPassiveAbilities(TriggerKeyword trigger, PlayerTarget concernedPlayer)
    {
        foreach (var slot in LocalPlayer.ActiveSlots)
        {
            List<Ability> triggered = GetAbilitiesWithTrigger(trigger, concernedPlayer, slot.Card);
            foreach (var ability in triggered)
            {
                ExecuteAbility(ability, slot.Card, LocalPlayer);
            }
        }
        
        foreach (var slot in Opponent.ActiveSlots)
        {
            List<Ability> triggered = GetAbilitiesWithTrigger(trigger, concernedPlayer == PlayerTarget.Opponent ? PlayerTarget.LocalPlayer : PlayerTarget.Opponent, slot.Card);
            foreach (var ability in triggered)
            {
                ExecuteAbility(ability, slot.Card, Opponent);
            }
        }
    }

    public bool ExecuteAbility(Ability ability, Card fromCard, PlayerController fromPlayer)
    {
        if (!TestAbilityConditions(ability.Condition))
            return false;

        if (fromCard.Type != CardType.Parchment)
        {
            if (fromCard.Element == Element.Light)
            {
                AnimationManager.Instance.AppendAnim(new AnimationTemplate(AnimationManager.Instance.ProcLightCard, new List<Card>(){fromCard}, 0f, 0f));
            }
        
            if (fromCard.Element == Element.Water)
            {
                AnimationManager.Instance.AppendAnim(new AnimationTemplate(AnimationManager.Instance.ProcWaterCard, new List<Card>(){fromCard}, 0f, 0f));
            }
            
            if (fromCard.Element == Element.Shadow)
            {
                AnimationManager.Instance.AppendAnim(new AnimationTemplate(AnimationManager.Instance.ProcDarkCard, new List<Card>(){fromCard}, 0f, 0f));
            }
        }

        for (int i = 0; i < ability.RepeatCounter; i++)
        {
            //Damage dealing
            if (ability.AbilityName == AbilityName.IncreaseLifePoints)
            {
                ApplyAbilityToPlayers(ability.PlayerTarget, IncreaseLifePointsAbility, fromCard, fromPlayer, ability);
            }
                
            //Sanity modifications
            if (ability.AbilityName == AbilityName.IncreaseSanity)
            {
                ApplyAbilityToPlayers(ability.PlayerTarget, IncreaseSanityAbility, fromCard, fromPlayer, ability);
            }
            
            //Mana modifications
            if (ability.AbilityName == AbilityName.IncreaseMana)
            {
                ApplyAbilityToPlayers(ability.PlayerTarget, IncreaseManaAbility, fromCard, fromPlayer, ability);
            }
            
            //Coins modifications
            if (ability.AbilityName == AbilityName.IncreaseCoins)
            {
                ApplyAbilityToPlayers(ability.PlayerTarget, IncreaseCoinsAbility, fromCard, fromPlayer, ability);
            }
                
            //Recovering cards from discard pile
            if (ability.AbilityName == AbilityName.PlaceCardInHand)
            {
                ApplyAbilityToPlayers(ability.PlayerTarget, PlaceCardsInHand, fromCard, fromPlayer, ability);
            }
            
            //Swapping card buy cost and play cost
            if (ability.AbilityName == AbilityName.SwapPlayCostWithBuyCost)
            {
                ApplyAbilityToPlayers(ability.PlayerTarget, SwapPlayCostWithBuyCost, fromCard, fromPlayer, ability);
            }
            
            //Enable Spend Madness Instead Of Mana
            if (ability.AbilityName == AbilityName.EnableSpendMadnessInsteadOfMana)
            {
                ApplyAbilityToPlayers(ability.PlayerTarget, EnableSpendMadnessInsteadOfMana, fromCard, fromPlayer, ability);
            }
            
            //Enable Spend Madness Instead Of Mana
            if (ability.AbilityName == AbilityName.EnableLoseAllCoinsAtTurnEnd)
            {
                ApplyAbilityToPlayers(ability.PlayerTarget, EnableLoseAllCoinsAtTurnEnd, fromCard, fromPlayer, ability);
            }
            
            //Increase card price
            if (ability.AbilityName == AbilityName.IncreaseCardPrice)
            {
                ApplyAbilityToPlayers(ability.PlayerTarget, IncreaseCardPrice, fromCard, fromPlayer, ability);
            }
            
            //Steal coins
            if (ability.AbilityName == AbilityName.StealCoins)
            {
                ApplyAbilityToPlayers(ability.PlayerTarget, StealCoins, fromCard, fromPlayer, ability);
            }
            
            //Set play cost
            if (ability.AbilityName == AbilityName.SetPlayCost)
            {
                ApplyAbilityToPlayers(ability.PlayerTarget, SetPlayCost, fromCard, fromPlayer, ability);
            }
            
            //Set play cost
            if (ability.AbilityName == AbilityName.IncreasePlayCost)
            {
                ApplyAbilityToPlayers(ability.PlayerTarget, IncreasePlayCost, fromCard, fromPlayer, ability);
            }
            
            //Send to discard pile
            if (ability.AbilityName == AbilityName.SendToDiscardPile)
            {
                ApplyAbilityToPlayers(ability.PlayerTarget, SendToDiscardPile, fromCard, fromPlayer, ability);
            }

            //Increase cast cost temporarily
            if (ability.AbilityName == AbilityName.ResetSpellSlot)
            {
                ApplyAbilityToPlayers(ability.PlayerTarget, ResetSpellSlot, fromCard, fromPlayer, ability);
            }
            
            //Increment market
            if (ability.AbilityName == AbilityName.IncrementMarket)
            {
                ApplyAbilityToPlayers(ability.PlayerTarget, IncrementMarketAbility, fromCard, fromPlayer, ability);
            }
            
            //Swap play cost of 2 cards
            if (ability.AbilityName == AbilityName.SwapPlayCost)
            {
                ApplyAbilityToPlayers(ability.PlayerTarget, SwapPlayCostOf2Cards, fromCard, fromPlayer, ability);
            }
            
            //Win the game
            if (ability.AbilityName == AbilityName.WinTheGame)
            {
                ApplyAbilityToPlayers(ability.PlayerTarget, WinTheGameAbility, fromCard, fromPlayer, ability);
            }
            
            //Disable spell slot
            if (ability.AbilityName == AbilityName.DisableSpell)
            {
                ApplyAbilityToPlayers(ability.PlayerTarget, DisableSpell, fromCard, fromPlayer, ability);
            }
        }

        return true;
    }

    public void ExecuteCardAbilities(Card fromCard, PlayerController fromPlayer)
    {
        foreach (var ability in fromCard.Abilities)
        {
            ExecuteAbility(ability, fromCard, fromPlayer);
        }
    }

    public bool TestAbilityConditions(AbilityCondition condition)
    {
        if (!condition)
            return true;
        
        List<Card> coTargets = RetrieveTargets(condition.CardTargets, condition.PlayerTargetModifier, condition.CardTargetFilters, condition.CardTargetExcluders, condition.RandomFromTargets, condition.RandomFromEachTarget);
        PlayerController playerTarget = GetPlayerFromPlayerTarget(condition.PlayerTargetModifier, IsLocalPlayerTurn() ? LocalPlayer : Opponent);

        if (condition.Type == ConditionType.CardPlayCost)
        {
            foreach (var card in coTargets)
            {
                if (card.PlayCost != condition.PlayCostModifier)
                    return false;
            }
        }
        
        if (condition.Type == ConditionType.CardElement)
        {
            foreach (var card in coTargets)
            {
                if (card.Element != condition.ElementModifier)
                    return false;
            }
        }
        
        if (condition.Type == ConditionType.CardType)
        {
            foreach (var card in coTargets)
            {
                if (card.Type != condition.TypeModifier)
                    return false;
            }
        }

        if (condition.Type == ConditionType.CoinsAmount)
        {
            return condition.TestOrdination(playerTarget.Coins);
        }
        
        if (condition.Type == ConditionType.Sanity)
        {
            return condition.TestOrdination(playerTarget.Sanity);
        }
        
        if (condition.Type == ConditionType.CardCount)
        {
            return condition.TestOrdination(coTargets.Count);
        }

        return true;
    }

    public void ApplyAbilityToPlayers(PlayerTarget target, Action<PlayerController, Card, PlayerController, Ability> func, Card fromCard, PlayerController fromPlayer, Ability ability)
    {
        if (target == PlayerTarget.None)
        {
            func(LocalPlayer, fromCard, fromPlayer, ability);
        }
        else if (target == PlayerTarget.Both)
        {
            func(LocalPlayer, fromCard, fromPlayer, ability);
            func(Opponent, fromCard, fromPlayer, ability);
        }
        else
        {
            PlayerController playerTarget = GetPlayerFromPlayerTarget(target, fromPlayer);
            func(playerTarget, fromCard, fromPlayer, ability);
        }
    }

    public PlayerController GetPlayerFromPlayerTarget(PlayerTarget playerTarget, PlayerController fromPlayer)
    {
        if (playerTarget == PlayerTarget.LocalPlayer)
        {
            if (fromPlayer == LocalPlayer)
                return LocalPlayer;

            if (fromPlayer == Opponent)
                return Opponent;
        }
            
        if (playerTarget == PlayerTarget.Opponent)
        {
            if (fromPlayer == LocalPlayer)
                return Opponent;

            if (fromPlayer == Opponent)
                return LocalPlayer;
        }

        return null;
    }

    public List<Card> FilterCardsFromPool(List<Card> pool, RichModifiers filters, RichModifiers excluders)
    {
        if (!filters)
            return pool;

        if (excluders != null)
        {
            List<Card> excludePool = RetrieveTargets(excluders.CardTargets, excluders.PlayerTargetModifier, excluders.CardTargetFilters, excluders.CardTargetExcluders, excluders.RandomFromTargets, excluders.RandomFromEachTarget);
            foreach (var card in excludePool)
            {
                pool.Remove(card);
            }
        }
        
        return pool.Where(c => (String.IsNullOrEmpty(filters.StringModifier) || c.CodeName == filters.StringModifier)
                               && (filters.PlayCostModifier < 0 || c.PlayCost == filters.PlayCostModifier)
                               && (filters.BuyCostModifier < 0 || c.BuyCost == filters.BuyCostModifier)
                               && (filters.ElementModifier == Element.None || c.Element == filters.ElementModifier)
                               && (filters.TypeModifier == CardType.None || c.Type == filters.TypeModifier)).ToList();
    }

    public List<Card> RetrieveAbilityCardTargets(Ability ability)
    {
        return RetrieveTargets(ability.CardTargets, ability.PlayerTargetModifier, ability.CardTargetFilters, ability.CardTargetExcluders, ability.RandomFromTargets, ability.RandomFromEachTarget);
    }

    public List<Card> RetrieveTargets(List<CardTarget> targetModifiers, PlayerTarget playerTargetModifier, RichModifiers targetFilters, RichModifiers targetExcluders, bool randomFromAll, bool randomFromEach)
    {
        List<Card> targets = new List<Card>();
        PlayerController playerTarget = GetPlayerFromPlayerTarget(playerTargetModifier, IsLocalPlayerTurn() ? LocalPlayer : Opponent);

        foreach (var targetModifier in targetModifiers)
        {
            List<Card> addedTargets = new List<Card>();
            if (targetModifier == CardTarget.LastCardAddedToHand && playerTarget.LastAddedCardToHand != null)
                addedTargets.Add(playerTarget.LastAddedCardToHand);
            
            if (targetModifier == CardTarget.DiscardPile)
                addedTargets.AddRange(playerTarget.DiscardPile);
            
            if (targetModifier == CardTarget.Market)
                addedTargets.AddRange(Market);
            
            if (targetModifier == CardTarget.PlayedThisTurn)
                addedTargets.AddRange(playerTarget.PlayedThisTurn);
            
            if (targetModifier == CardTarget.Hand)
                addedTargets.AddRange(playerTarget.Hand);
            
            if (targetModifier == CardTarget.InPlaySpells)
                addedTargets.AddRange(playerTarget.ActiveSlots.Select(s => s.Card));
            
            if (targetModifier == CardTarget.LastBoughtCard)
                addedTargets.Add(playerTarget.LastBoughtCard);
            
            if (targetModifier == CardTarget.LastCastSpell)
                addedTargets.Add(playerTarget.LastCastSpell);
            
            if (targetModifier == CardTarget.LastPlayedCard && playerTarget.LastPlayedCard != null)
                addedTargets.Add(playerTarget.LastPlayedCard);
            
            if (targetModifier == CardTarget.TopDiscarded && playerTarget.DiscardPile.Count > 0)
                addedTargets.Add(playerTarget.DiscardPile[playerTarget.DiscardPile.Count - 1]);

            if (randomFromEach && addedTargets.Count > 0)
            {
                Card added = addedTargets.RandomItem();
                Debug.Log("New state : " + Random.state);
                bool targetsContainsAll = true;
                foreach (var t in addedTargets)
                {
                    if (!targets.Contains(t))
                        targetsContainsAll = false;
                }

                while (!targetsContainsAll && targets.Contains(added))
                {
                    added = addedTargets.RandomItem();
                    Debug.Log("New state : " + Random.state);
                }
                
                targets.Add(added);
            }
            else
            {
                targets.AddRange(addedTargets);
            }
        }

        targets = FilterCardsFromPool(targets, targetFilters, targetExcluders);
        
        if (randomFromAll && targets.Count > 0)
            targets = new List<Card>() {targets.RandomItem()};

        return targets;
    }

    public void IncreaseLifePointsAbility(PlayerController target, Card fromCard, PlayerController fromPlayer, Ability ability)
    {
        int amount = ability.GetAmount();
        if (amount < 0)
        {
            foreach (var slot in fromPlayer.ActiveSlots)
            {
                var fireRune = GetAbilityWithName(AbilityName.AddDamageToFireSpells, slot.Card);
                if (fireRune && fromCard.Element == Element.Fire)
                    amount += fireRune.IntModifier;
            }
            
            foreach (var slot in target.ActiveSlots)
            {
                var earthProtector = GetAbilityWithName(AbilityName.ReduceDamages, slot.Card);
                if (earthProtector)
                    amount = Mathf.Min(earthProtector.IntModifier + amount, 0);
            }
        }

        IncreasePlayerLifePoints(target, amount, fromCard.Element);
    }
    
    public void IncreaseSanityAbility(PlayerController target, Card fromCard, PlayerController fromPlayer, Ability ability)
    {
        IncreasePlayerSanity(target, ability.GetAmount());
    }
    
    public void IncreaseCoinsAbility(PlayerController target, Card fromCard, PlayerController fromPlayer, Ability ability)
    {
        IncreasePlayerCoins(target, ability.GetAmount());
    }
    
    public void IncreaseManaAbility(PlayerController target, Card fromCard, PlayerController fromPlayer, Ability ability)
    {
        IncreasePlayerMana(target, ability.GetAmount());
    }

    public void PlaceCardsInHand(PlayerController target, Card fromCard, PlayerController fromPlayer, Ability ability)
    {
        List<Card> targets = RetrieveAbilityCardTargets(ability);

        int nbIncrMarket = 0;
        
        foreach (var card in targets)
        {
            LocalPlayer.DiscardPile.Remove(card);
            Opponent.DiscardPile.Remove(card);
            LocalPlayer.Hand.Remove(card);
            Opponent.Hand.Remove(card);

            if (Market.Contains(card))
            {
                Market.Remove(card);
                nbIncrMarket++;
            }
            
            target.Hand.Add(card);
            target.LastAddedCardToHand = card;
        }

        for (int i = 0; i < nbIncrMarket; i++)
        {
            IncrementMarket();
        }
        
        UIControllerForGame.Instance.RearangeDiscardPile(true);
        UIControllerForGame.Instance.RearangeDiscardPile(false);
        UIControllerForGame.Instance.RearangeHand(Opponent);
        UIControllerForGame.Instance.RearangeHand(LocalPlayer);
    }
    
    public void SwapPlayCostWithBuyCost(PlayerController target, Card fromCard, PlayerController fromPlayer, Ability ability)
    {
        List<Card> targets = RetrieveAbilityCardTargets(ability);
        
        foreach (var card in targets)
        {
            (card.BuyCost, card.PlayCost) = (card.PlayCost, card.BuyCost);
            card.UpdateCostVisuals();
        }
    }
    
    public void IncreaseCardPrice(PlayerController target, Card fromCard, PlayerController fromPlayer, Ability ability)
    {
        List<Card> targets = RetrieveAbilityCardTargets(ability);
        foreach (var card in targets)
        {
            card.BuyCost = Mathf.Max(card.BuyCost + ability.GetAmount(), 0);
        }
    }
    
    public void EnableSpendMadnessInsteadOfMana(PlayerController target, Card fromCard, PlayerController fromPlayer, Ability ability)
    {
        target.SpendMadnessInsteadOfMana = true;
    }
    
    public void EnableLoseAllCoinsAtTurnEnd(PlayerController target, Card fromCard, PlayerController fromPlayer, Ability ability)
    {
        target.LoseAllCoinsAtTurnEnd = true;
    }
    
    public void StealCoins(PlayerController target, Card fromCard, PlayerController fromPlayer, Ability ability)
    {
        int amount = ability.GetAmount();
        if (target.Coins < amount)
            amount = target.Coins;
        
        IncreasePlayerCoins(target, -amount);
        IncreasePlayerCoins(fromPlayer, amount);
    }
    
    public void SetPlayCost(PlayerController target, Card fromCard, PlayerController fromPlayer, Ability ability)
    {
        List<Card> targets = RetrieveAbilityCardTargets(ability);
        foreach (var card in targets)
        {
            card.PlayCost = Mathf.Max(ability.GetAmount(), 0);
        }
    }
    
    public void IncreasePlayCost(PlayerController target, Card fromCard, PlayerController fromPlayer, Ability ability)
    {
        List<Card> targets = RetrieveAbilityCardTargets(ability);
        foreach (var card in targets)
        {
            card.PlayCost = Mathf.Max(card.PlayCost + ability.GetAmount(), 0);
        }
    }

    public void ResetSpellSlot(PlayerController target, Card fromCard, PlayerController fromPlayer, Ability ability)
    {
        List<Card> targets = RetrieveAbilityCardTargets(ability);
        foreach (var card in targets)
        {
            SpellSlot slot = GetSlotContainingCard(card);
            slot.Reset();
        }
    }
    
    public void DisableSpell(PlayerController target, Card fromCard, PlayerController fromPlayer, Ability ability)
    {
        List<Card> targets = RetrieveAbilityCardTargets(ability);
        foreach (var card in targets)
        {
            SpellSlot slot = GetSlotContainingCard(card);
            slot.Use();
        }
    }
    
    public void SendToDiscardPile(PlayerController target, Card fromCard, PlayerController fromPlayer, Ability ability)
    {
        List<Card> targets = RetrieveAbilityCardTargets(ability);
        foreach (var card in targets)
        {
            LocalPlayer.DiscardPile.Remove(card);
            Opponent.DiscardPile.Remove(card);
            LocalPlayer.Hand.Remove(card);
            Opponent.Hand.Remove(card);
            
            target.DiscardPile.Add(card);
            card.ResetOutlines();
            AnimationManager.Instance.AppendAnim(new AnimationTemplate(AnimationManager.Instance.DiscardCard,
                new List<Card>() {card}, 0.5f, 0f, target == LocalPlayer, null, ComputeDiscardPileHeight(target == LocalPlayer, card)));
            UIControllerForGame.Instance.RearangeHand(target);
        }
    }
    
    public void IncrementMarketAbility(PlayerController target, Card fromCard, PlayerController fromPlayer, Ability ability)
    {
        int amount = ability.GetAmount();
        for (int i = 0; i < Mathf.Abs(amount); i++)
        {
            if (amount > 0)
                IncrementMarket();
            else
                DecrementMarket();
        }
    }
    
    public void SwapPlayCostOf2Cards(PlayerController target, Card fromCard, PlayerController fromPlayer, Ability ability)
    {
        List<Card> targets = RetrieveAbilityCardTargets(ability);

        (targets[0].PlayCost, targets[1].PlayCost) = (targets[1].PlayCost, targets[0].PlayCost);
        foreach (var card in targets)
        {
            card.UpdateCostVisuals();
        }
    }

    public void WinTheGameAbility(PlayerController target, Card fromCard, PlayerController fromPlayer, Ability ability)
    {
        PlayerWinGame(target.Username);
    }
    
    

    #endregion

    #region Pun Callbacks

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);
        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene(0);
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        SceneManager.LoadScene(0);
    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        SceneManager.LoadScene(0);
    }

    #endregion
    
    #region Useful Methods

    public float ComputeComplexAmount(ComplexAmount ca)
    {
        float lhs = ca.IntModifier;

        if (ca.FloatModifier != 0f)
            lhs = ca.FloatModifier;

        List<Card> caTargets = RetrieveTargets(ca.CardTargets, ca.PlayerTargetModifier, ca.CardTargetFilters, ca.CardTargetExcluders, ca.RandomFromTargets, ca.RandomFromEachTarget);
        PlayerController playerTarget = GetPlayerFromPlayerTarget(ca.PlayerTargetModifier, IsLocalPlayerTurn() ? LocalPlayer : Opponent);

        if (ca.Type == ComplexAmountType.BuyCost && caTargets.Count > 0)
            lhs = caTargets[0].BuyCost;
        
        if (ca.Type == ComplexAmountType.PlayCost && caTargets.Count > 0)
            lhs = caTargets[0].PlayCost;

        if (ca.Type == ComplexAmountType.CardCount)
            lhs = caTargets.Count;
        
        if (ca.Type == ComplexAmountType.Coins)
            lhs = playerTarget.Coins;

        if (ca.OperationWithNestedCA == Operation.Add)
            lhs += ComputeComplexAmount(ca.ComplexAmount);
        
        if (ca.OperationWithNestedCA == Operation.Mult)
            lhs *= ComputeComplexAmount(ca.ComplexAmount);

        return lhs;
    }

    public Card GetCardFromMarketPool(Guid id)
    {
        foreach (var card in MarketPool)
        {
            if (card.Id == id)
                return card;
        }

        return null;
    }

    public Card GetCardPrefabFromCodename(string code)
    {
        foreach (var card in AllCards)
        {
            if (card.CodeName == code)
                return card;
        }

        return null;
    }

    public float ComputeDiscardPileHeight(bool isLocal, Card card)
    {
        if (isLocal)
            return LocalPlayer.DiscardPile.IndexOf(card) * DiscardedCardThickness;
        else
        {
            return Opponent.DiscardPile.IndexOf(card) * DiscardedCardThickness;
        }
    }

    public bool IsLocalPlayer(PlayerController player)
    {
        return player == LocalPlayer;
    }

    public bool IsLocalPlayer(string username)
    {
        return username == LocalPlayer.Username;
    }
    
    public bool IsLocalPlayerTurn()
    {
        return _localPlayerTurn;
    }

    public bool IsOneLocalPlayerSlotFree()
    {
        foreach (var slot in SpellSlotsForLocalPlayer)
        {
            if (!slot.Card)
                return true;
        }

        return false;
    }

    public bool IsCardInMarket(Card card)
    {
        return Market.Contains(card);
    }

    public bool IsCardInMulligan(Card card)
    {
        return LocalMulligan.Contains(card);
    }

    public bool IsInLocalPlayerHand(Card card)
    {
        return LocalPlayer.Hand.Contains(card);
    }

    public bool IsSlotOwnedByLocalPlayer(SpellSlot slot)
    {
        return SpellSlotsForLocalPlayer.Contains(slot);
    }

    public bool IsMulligan()
    {
        return _mulligan;
    }

    public bool SelectedEnoughCardsInMulligan()
    {
        return LocalMulligan.Count(card => card.SelectedInMulligan) == 4;
    }

    public bool OpponentCompletedMulligan()
    {
        return _opponentMulliganValidated;
    }
    
    public bool LocalCompletedMulligan()
    {
        return _localMulliganValidated;
    }

    public SpellSlot GetFirstFreeSlot(bool forLocal)
    {
        if (forLocal)
        {
            foreach (var slot in SpellSlotsForLocalPlayer)
            {
                if (!slot.Card)
                    return slot;
            }
        }
        else
        {
            foreach (var slot in SpellSlotsForOpponent)
            {
                if (!slot.Card)
                    return slot;
            }
        }

        return null;
    }

    public SpellSlot GetSlotContainingCard(Card card)
    {
        foreach (var slot in SpellSlotsForOpponent)
        {
            if (slot.Card == card)
                return slot;
        }
        
        foreach (var slot in SpellSlotsForLocalPlayer)
        {
            if (slot.Card == card)
                return slot;
        }

        return null;
    }

    public void ResetSlots(bool forLocal)
    {
        if (forLocal)
        {
            foreach (var slot in SpellSlotsForLocalPlayer)
            {
                slot.Reset();
            }
        }
        else
        {
            foreach (var slot in SpellSlotsForOpponent)
            {
                slot.Reset();
            }
        }
    }

    public bool CanBuyCard(PlayerController player, Card card)
    {
        return player.Coins >= card.BuyCost && player.Hand.Count < MaxCardsInHand;
    }

    public bool CanPlayCard(PlayerController player, Card card)
    {
        if (!_localPlayerTurn)
            return false;
        
        if (player.Mana < card.PlayCost && !player.SpendMadnessInsteadOfMana)
            return false;

        if (card.Type != CardType.Parchment && !IsOneLocalPlayerSlotFree())
            return false;

        return true;
    }

    public bool CanCastSpell(PlayerController player, Card card)
    {
        foreach (var ability in card.Abilities)
        {
            if (!TestAbilityConditions(ability.Condition))
                return false;
        }

        return true;
    }

    public bool CanSeeOpponentStats()
    {
        foreach (var slot in SpellSlotsForOpponent)
        {
            if (slot.Card && slot.Card.HasAbility(AbilityName.HideStats))
                return false;
        }

        return true;
    }

    public Ability GetAbilityWithName(AbilityName name, Card card)
    {
        foreach (var ability in card.Abilities)
        {
            if (ability.AbilityName == name)
                return ability;
        }

        return null;
    }
    
    public List<Ability> GetAbilitiesWithTrigger(TriggerKeyword trigger, PlayerTarget targetModifier, Card card)
    {
        List<Ability> res = new List<Ability>();
        foreach (var ability in card.Abilities)
        {
            if (ability.Trigger == trigger && ability.TriggerPlayerTargetModifier == targetModifier)
                res.Add(ability);
        }

        return res;
    }

    public void IncreasePlayerSanity(PlayerController player, int amount)
    {
        int newSanity = player.Sanity + amount;
        if (newSanity != player.Sanity)
        {
            PlayerStatsUI stats = player == LocalPlayer
                ? UIControllerForGame.Instance.LocalPlayerStats
                : UIControllerForGame.Instance.OpponentPlayerStats;
            stats.UpdateSanity(newSanity, newSanity >= player.Sanity);
        }

        player.Sanity = newSanity;
        
        if (amount < 0)
            TriggerPassiveAbilities(TriggerKeyword.IncreasedMadness, player == LocalPlayer ? PlayerTarget.LocalPlayer : PlayerTarget.Opponent);
    }
    
    public void IncreasePlayerCoins(PlayerController player, int amount)
    {
        int newCoins = Mathf.Max(player.Coins + amount, 0);
        if (newCoins != player.Coins)
        {
            PlayerStatsUI stats = player == LocalPlayer
                ? UIControllerForGame.Instance.LocalPlayerStats
                : UIControllerForGame.Instance.OpponentPlayerStats;
            stats.UpdateCoins(newCoins, player.CoinGrowth,
                newCoins > player.Coins);
        }

        player.Coins = newCoins;
    }
    
    public void IncreasePlayerMana(PlayerController player, int amount)
    {
        int newMana = Mathf.Max(player.Mana + amount, 0);
        if (newMana != player.Mana)
        {
            PlayerStatsUI stats = player == LocalPlayer
                ? UIControllerForGame.Instance.LocalPlayerStats
                : UIControllerForGame.Instance.OpponentPlayerStats;
            stats.UpdateMana(newMana, player.ManaGrowth, newMana > player.Mana);
        }

        player.Mana = newMana;
    }
    
    public void IncreasePlayerLifePoints(PlayerController player, int amount, Element damageType)
    {
        int newLp = player.LifePoints + amount;
        if (newLp != player.Mana)
        {
            PlayerStatsUI stats = player == LocalPlayer
                ? UIControllerForGame.Instance.LocalPlayerStats
                : UIControllerForGame.Instance.OpponentPlayerStats;
            stats.UpdateLifePoints(newLp, newLp > player.LifePoints, damageType);
        }

        player.LifePoints += amount;
    }

    public bool NoMoreActionPossible()
    {
        if (!IsLocalPlayerTurn())
            return false;

        if (LocalPlayer.SpendMadnessInsteadOfMana)
        {
            if (LocalPlayer.Hand.Count(c => c.Type != CardType.Parchment) > 0 && IsOneLocalPlayerSlotFree())
                return false;
            
            if (LocalPlayer.Hand.Count(c => c.Type == CardType.Parchment) > 0)
                return false;
        }
            
        foreach (var card in LocalPlayer.Hand)
        {
            if (card.PlayCost <= LocalPlayer.Mana && IsOneLocalPlayerSlotFree())
                return false;
        }

        foreach (var slot in SpellSlotsForLocalPlayer)
        {
            if (slot.Card && !slot.IsUsed() && slot.Card.Type == CardType.Spell)
                return false;
        }

        foreach (var card in Market)
        {
            if (Market.IndexOf(card) != 0 && Market.IndexOf(card) != 4 && card.BuyCost <= LocalPlayer.Coins)
                return false;
        }

        return true;
    }

    #endregion
    
}
