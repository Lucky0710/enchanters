using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using Coffee.UIExtensions;
using DG.Tweening;
using Unity.IO.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIControllerForGame : MonoBehaviour
{
    public static UIControllerForGame Instance => _instance;
    private static UIControllerForGame _instance;

    private void Awake()
    {
        _instance = this;
    }

    public PlayerStatsUI LocalPlayerStats;
    public PlayerStatsUI OpponentPlayerStats;
    
    public Text EndTurnButtonText;
    public Image EndTurnButtonBG;
    public UIParticle EndTurnButtonParticles;
    public GameObject EndTurnButtonGlow;

    public GameObject EndScreenRoot;
    public Text EndScreenTitle;
    public Text EndScreenPlayerWonText;
    public Image EndScreenDarkBG;

    public MeshRenderer DarkBgAboveBoard;
    public GameObject MulliganText;

    public float CardInHandSize;
    public float CardDetailSize;
    public float CardDetailYPos;
    public float CardDetailZPos;

    public Vector3 CardSlotDetailOffset;
    public Vector3 CardDetailStartPosOffset;
    public float CardSlotDetailZPos;

    private Card _cardDetailForSlot;

    public void OnEndTurnClicked()
    {
        if (MatchController.Instance.IsMulligan() && MatchController.Instance.SelectedEnoughCardsInMulligan())
        {
            MatchController.Instance.ValidateMulligan();
            EndTurnButtonParticles.Play();
        }
        else if (MatchController.Instance.IsLocalPlayerTurn())
        {
            MatchController.Instance.EndLocalPlayerTurn();
            EndTurnButtonParticles.Play();
        }
        
        UpdateUI();
        
    }

    public void OnReturnToMenuClicked()
    {
        SceneManager.LoadScene(0);
    }

    public void UpdateUI()
    {
        UpdateEndTurnButtonDisplay(MatchController.Instance.IsLocalPlayerTurn(), MatchController.Instance.IsMulligan());
        UpdateHandOutlines();
        UpdateMarketOutlines(MatchController.Instance.IsLocalPlayerTurn());
        UpdateSpellSlotsOutlines(MatchController.Instance.IsLocalPlayerTurn());
        OpponentPlayerStats.UpdateDisplay(MatchController.Instance.CanSeeOpponentStats());
    }

    public void SetPlayersUI(PlayerController local, PlayerController opponent)
    {
        LocalPlayerStats.InitPlayer(local);
        OpponentPlayerStats.InitPlayer(opponent);
    }
    

    #region UI Management
    
    private void UpdateEndTurnButtonDisplay(bool myTurn, bool mulligan)
    {
        if (myTurn)
        {
            EndTurnButtonText.text = "End Turn";
            EndTurnButtonBG.color = Color.green;
            EndTurnButtonGlow.SetActive(MatchController.Instance.NoMoreActionPossible());
        }
        else
        {
            EndTurnButtonText.text = "Opponent Turn";
            EndTurnButtonBG.color = Color.gray;
            EndTurnButtonGlow.SetActive(false);
        }

        if (mulligan)
        {
            if (MatchController.Instance.LocalCompletedMulligan())
            {
                if (!MatchController.Instance.OpponentCompletedMulligan())
                {
                    EndTurnButtonText.text = "Waiting for Opponent";
                    EndTurnButtonGlow.SetActive(false);
                    EndTurnButtonBG.color = Color.gray;
                }
                else
                {
                    EndTurnButtonText.text = "Starting...";
                    EndTurnButtonGlow.SetActive(false);
                    EndTurnButtonBG.color = Color.gray;
                }
            }
            else
            {
                EndTurnButtonText.text = "Confirm";
                EndTurnButtonGlow.SetActive(MatchController.Instance.SelectedEnoughCardsInMulligan());
                EndTurnButtonBG.color = MatchController.Instance.SelectedEnoughCardsInMulligan() ? Color.green : Color.grey;
            }
        }
    }

    public void InitEndScreen(bool won, string victoriousPlayerName)
    {
        EndScreenRoot.SetActive(true);
        EndScreenDarkBG.DOFade(0.8f, 1f);
        EndScreenTitle.text = won ? "Victory !" : "Defeat...";
        EndScreenTitle.color = won ? Color.yellow : Color.red;
        EndScreenTitle.transform.DOScale(1f, 1f).From(5f).SetEase(Ease.InCubic);
        EndScreenPlayerWonText.text = victoriousPlayerName + " won.";
    }

    public void InitMulliganScreen()
    {
        DarkBgAboveBoard.gameObject.SetActive(true);
        DarkBgAboveBoard.material.DOFade(0.5f, 1f);
        MulliganText.SetActive(true);
    }

    public void DismissMulliganScreen()
    {
        DarkBgAboveBoard.material.DOFade(0f, 1f).OnComplete(() =>
        {
            DarkBgAboveBoard.gameObject.SetActive(false);
        });
        MulliganText.SetActive(false);
    }

    #endregion


    #region Card Async Movements

    
    public void HooverCardInHand(Card card, bool first)
    {
        card.VisualRoot.DOKill();
        if (first)
        {
            card.VisualRoot.DOScale(Vector3.one * CardDetailSize, 0.1f).SetEase(Ease.OutQuad);
            card.VisualRoot.DOMove(new Vector3(card.transform.position.x, CardDetailYPos, CardDetailZPos), 0.1f).SetEase(Ease.OutQuad);
            card.VisualRoot.DORotate(Vector3.zero, 0.1f).SetEase(Ease.OutQuad);
        }
        else
        {
            card.VisualRoot.localScale = Vector3.one * CardDetailSize;
            card.VisualRoot.position = new Vector3(card.transform.position.x, CardDetailYPos, CardDetailZPos);
            card.VisualRoot.eulerAngles = Vector3.zero;
        }
        
        if (MatchController.Instance.CanPlayCard(MatchController.Instance.LocalPlayer, card))
        {
            card.PlayableOutlineHoovered.SetActive(true);
        }
    }
    
    public void UnhooverCardInHand(Card card)
    {
        card.VisualRoot.DOScale(1f, 0.4f).SetEase(Ease.OutQuad);
        card.VisualRoot.DOLocalMove(Vector3.zero, 0.4f)
            .SetEase(Ease.OutQuad);
        card.VisualRoot.DOLocalRotate(new Vector3(0f, 0f, 0f), 0.4f).SetEase(Ease.OutQuad);
        card.PlayableOutlineHoovered.SetActive(false);
    }
    
    public void HooverCardInMarket(Card card)
    {
        if (MatchController.Instance.CanBuyCard(MatchController.Instance.LocalPlayer, card))
        {
            card.BuyableOutlineHoovered.SetActive(true);
        }
    }
    
    public void UnhooverCardInMarket(Card card)
    {
        card.BuyableOutlineHoovered.SetActive(false);
    }
    
    public void HooverCardInMulligan(Card card)
    {
        card.BuyableOutlineHoovered.SetActive(true);
    }
    
    public void UnhooverCardInMulligan(Card card)
    {
        card.BuyableOutlineHoovered.SetActive(false);
    }

    public void HoldCard(Card card)
    {
        card.VisualRoot.DOKill();
        card.transform.DOKill();
        card.transform.localScale = Vector3.one * CardDetailSize * CardInHandSize;
        card.transform.position = new Vector3(card.transform.position.x, CardDetailYPos, CardDetailZPos);
        card.transform.eulerAngles = Vector3.zero;
        card.VisualRoot.localScale = Vector3.one;
        card.VisualRoot.localPosition = Vector3.zero;
        card.VisualRoot.localEulerAngles = Vector3.zero;
    }

    public void PlaceCardInHand(bool isLocal, Card card)
    {
        card.transform.DOKill();
        if (isLocal)
        {
            Vector3 endPos = MatchController.Instance.LocalPlayer.ComputeCardPositionInHand(card, true) + AnimationManager.Instance.LocalPlayerHandRoot.position;
            card.transform.DOMove(endPos, 0.3f).SetEase(Ease.OutQuad);
            float tilt = MatchController.Instance.LocalPlayer.ComputeCardTiltInHand(card);
            card.transform.DORotate(new Vector3(0f, 0f, tilt), 0.3f);
            card.transform.DOScale(CardInHandSize, 0.3f).SetEase(Ease.OutQuad);
        }
        else
        {
            Vector3 endPos = MatchController.Instance.Opponent.ComputeCardPositionInHand(card, false) + AnimationManager.Instance.OpponentHandRoot.position;
            card.transform.DOMove(endPos, 0.3f).SetEase(Ease.OutQuad);
            float tilt = MatchController.Instance.LocalPlayer.ComputeCardTiltInHand(card);
            card.transform.DORotate(new Vector3(0f, 180f, tilt), 0.3f);
            card.transform.DOScale(CardInHandSize, 0.3f).SetEase(Ease.OutQuad);
        }
    }
    
    public void ReplaceCardInMarketAtRelease(Card card)
    {
        int marketId = MatchController.Instance.Market.IndexOf(card);
        Vector3 endPos = AnimationManager.Instance.MarketSlots[marketId].position;
        card.transform.DOMove(endPos, 0.3f).SetEase(Ease.OutQuad).OnComplete(() =>
        {
            card.VisualRoot.localPosition = Vector3.zero;
        });
        
        if (MatchController.Instance.Market.IndexOf(card) != 0
            && MatchController.Instance.Market.IndexOf(card) != 4)
        {
            card.BuyingOutline.SetActive(false);
            UpdateMarketOutlines(MatchController.Instance.IsLocalPlayerTurn());
        }
    }

    public void PlacePlayedCardToSide(Card card)
    {
        card.transform.DOScale(2f, 0.5f).SetEase(Ease.OutQuad);
        card.transform.DOMove(AnimationManager.Instance.PlayedCardRoot.transform.position, 0.5f).SetEase(Ease.OutCubic);
        card.transform.DORotate(new Vector3(0f, 0f, 0f), 0.2f).SetEase(Ease.OutQuad);
    }

    public void RearangeHand(PlayerController player)
    {
        foreach (var card in player.Hand)
        {
            card.transform.DOKill();
            Vector3 endPos;
            float tilt;
            
            if (MatchController.Instance.IsLocalPlayer(player))
            {
                endPos = player.ComputeCardPositionInHand(card, true) + AnimationManager.Instance.LocalPlayerHandRoot.position;
                tilt = MatchController.Instance.LocalPlayer.ComputeCardTiltInHand(card);
            }
            else
            {
                endPos = player.ComputeCardPositionInHand(card, false) + AnimationManager.Instance.OpponentHandRoot.position;
                tilt = MatchController.Instance.Opponent.ComputeCardTiltInHand(card);
            }
            
            card.transform.DOMove(endPos, 0.2f).SetEase(Ease.OutQuad);
            card.transform.DORotate(new Vector3(0f, player == MatchController.Instance.LocalPlayer ? 0f : 180f, tilt), 0.2f);
            card.transform.DOScale(CardInHandSize, 0.3f).SetEase(Ease.OutQuad);
        }
    }

    public void RearangeDiscardPile(bool forLocal)
    {
        var pile = forLocal
            ? MatchController.Instance.LocalPlayer.DiscardPile
            : MatchController.Instance.Opponent.DiscardPile;

        foreach (var card in pile)
        {
            var pos = forLocal
                ? AnimationManager.Instance.LocalDiscardPileRoot
                : AnimationManager.Instance.OpponentDiscardPileRoot;
            card.transform.DOMove(pos.position + new Vector3(0f, 0f, MatchController.Instance.ComputeDiscardPileHeight(forLocal, card)), 0.2f)
                .SetEase(Ease.OutQuad);
        }
    }

    #endregion


    #region Outlines Management

    
    public void EmphasizeOutlineForCard(GameObject outline)
    {
        outline.transform.DOComplete();
        outline.transform.DOPunchScale(new Vector3(0.7f, 0.7f, 0f), 0.3f, 0, 0f).SetEase(Ease.OutQuad);
    }

    private void UpdateHandOutlines()
    {
        foreach (var card in MatchController.Instance.LocalPlayer.Hand)
        {
            card.PlayableOutline.SetActive(MatchController.Instance.CanPlayCard(MatchController.Instance.LocalPlayer, card));
            card.BuyableOutline.SetActive(false);
            card.BuyingOutline.SetActive(false);
        }
    }
    
    private void UpdateMarketOutlines(bool isLocalPlayerTurn)
    {
        if (MatchController.Instance.Market.Count < 5)
            return;
        
        for (int i = 1; i < 4; i++)
        {
            MatchController.Instance.Market[i].BuyableOutline.SetActive(isLocalPlayerTurn && MatchController.Instance.Market[i].BuyCost <= MatchController.Instance.LocalPlayer.Coins);
        }
        
        MatchController.Instance.Market[0].BuyableOutline.SetActive(false);
        MatchController.Instance.Market[4].BuyableOutline.SetActive(false);
    }

    private void UpdateSpellSlotsOutlines(bool isLocalPlayerTurn)
    {
        foreach (var slot in MatchController.Instance.SpellSlotsForLocalPlayer)
        {
            slot.UpdateOutline(isLocalPlayerTurn && slot.Card && MatchController.Instance.CanCastSpell(MatchController.Instance.LocalPlayer, slot.Card));
        }
    }

    public void ShowSpellSlotDetails(SpellSlot slot)
    {
        HideSpellSlotDetails();

        _cardDetailForSlot = Instantiate(slot.Card, MatchController.Instance.CleanRoot);
        _cardDetailForSlot.gameObject.SetActive(true);
        _cardDetailForSlot.transform.position = slot.transform.position + CardDetailStartPosOffset;
        Plane plane = new Plane(Vector3.forward, CardSlotDetailZPos);
        Ray ray = Camera.main.ScreenPointToRay(Camera.main.WorldToScreenPoint(slot.transform.position));
        float enter = 0.0f;

        if (plane.Raycast(ray, out enter))
        {
            //Get the point that is clicked
            Vector3 hitPoint = ray.GetPoint(enter);
            Vector3 finalPos = hitPoint + CardSlotDetailOffset;
            _cardDetailForSlot.transform.DOKill();
            _cardDetailForSlot.transform.eulerAngles = new Vector3(0f, 0f, 0f);
            _cardDetailForSlot.transform.DOMove(finalPos, 0.15f).SetEase(Ease.OutQuad);
            _cardDetailForSlot.transform.DOScale(1f, 0.15f).From(0f).SetEase(Ease.OutQuad);
        }
    }

    public void HideSpellSlotDetails()
    {
        if (_cardDetailForSlot)
        {
            _cardDetailForSlot.transform.DOKill();
            Destroy(_cardDetailForSlot.gameObject);
        }
    }
    
    #endregion

}
