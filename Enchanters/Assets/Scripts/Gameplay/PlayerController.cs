using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

[RequireComponent(typeof(PhotonView))]
public class PlayerController : MonoBehaviourPunCallbacks
{
    public string Username;
    
    public int LifePoints;
    public int Mana;
    public int ManaGrowth;
    public int Coins;
    public int CoinGrowth;
    
    public int Sanity;

    public List<Card> Hand;
    public List<SpellSlot> ActiveSlots;
    public List<Card> DiscardPile;

    public Card LastAddedCardToHand;
    public Card LastPlayedCard;
    public Card LastBoughtCard;
    public Card LastCastSpell;
    public List<Card> PlayedThisTurn = new List<Card>();

    public bool SpendMadnessInsteadOfMana;
    public bool LoseAllCoinsAtTurnEnd;

    private void Start()
    {
        if (MatchController.Instance.LocalPlayer != this && MatchController.Instance.Online)
            MatchController.Instance.InitOpponent(this);
    }

    public void InitForMatch(int baseLifePoints, int baseMana, int baseCoins, int baseManaGrowth, int baseCoinGrowth)
    {
        LifePoints = baseLifePoints;
        Sanity = 0;
        Coins = baseCoins;
        Mana = baseMana;
        ManaGrowth = baseManaGrowth;
        CoinGrowth = baseCoinGrowth;
    }

    public void GainStatsBetweenTurns()
    {
        ManaGrowth += 1;
        CoinGrowth += 1;
        MatchController.Instance.IncreasePlayerMana(this, ManaGrowth - Mana);
        MatchController.Instance.IncreasePlayerCoins(this, CoinGrowth);
    }

    public Vector3 ComputeCardPositionInHand(Card card, bool isLocal)
    {
        int cardId = Hand.IndexOf(card);
        float mid = (Hand.Count - 1) / 2f;
        
        float minCardSpacing = 1f;
        float maxCardSpacing = 1.5f;
        float cardSpacing = Mathf.Lerp(maxCardSpacing, minCardSpacing, (float) Hand.Count / MatchController.Instance.MaxCardsInHand);
        float xPos = (cardId - mid) * cardSpacing;

        float minCardHeight = -2.1f;
        float cardHeightDiff = minCardHeight / MatchController.Instance.MaxCardsInHand;
        float yPos = Mathf.Min(Mathf.Abs(cardId - mid) * cardHeightDiff, -0.05f);
        yPos *= -yPos;
        float cardDepthDiff = -0.1f;
        float zPos = cardDepthDiff * cardId;

        return new Vector3(xPos, isLocal ? yPos : -yPos, zPos);
    }
    
    public float ComputeCardTiltInHand(Card card)
    {
        int cardId = Hand.IndexOf(card);
        float maxTilt = 25f;
        float midId = (Hand.Count - 1) / 2f;
        return (midId - cardId) * (maxTilt * 2f / MatchController.Instance.MaxCardsInHand);
    }

    public void ResetTurnEnchants()
    {
        SpendMadnessInsteadOfMana = false;
        PlayedThisTurn.Clear();

        if (LoseAllCoinsAtTurnEnd)
            Coins = 0;
        
        LoseAllCoinsAtTurnEnd = false;
    }

}
