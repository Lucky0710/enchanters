using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "Modifiers")]
public class RichModifiers : ScriptableObject
{
    [Header("Modifiers")] 
    public bool BoolModifier;
    public int IntModifier;
    public int BuyCostModifier = -1;
    public int PlayCostModifier = -1;
    public float FloatModifier;
    public string StringModifier;
    public TriggerKeyword TriggerKeywordModifier = TriggerKeyword.None;
    public AbilityName AbilityNamesModifier = AbilityName.None;
    public Element ElementModifier = Element.None;
    public CardType TypeModifier = CardType.None;
        
    public ComplexAmount ComplexAmount;
    public List<Ability> LinkedAbilities;
    public List<Card> LinkedCards;
    
    public PlayerTarget PlayerTargetModifier = PlayerTarget.None;
    public List<CardTarget> CardTargets;
    public RichModifiers CardTargetExcluders;
    public RichModifiers CardTargetFilters;
    public bool RandomFromTargets = false;
    public bool RandomFromEachTarget = false;
    
    public int GetAmount()
    {
        if (ComplexAmount)
        {
            int res = Mathf.CeilToInt(MatchController.Instance.ComputeComplexAmount(ComplexAmount));

            if (ComplexAmount.ClampModifier == ClampModifier.Max)
                return Mathf.Min(ComplexAmount.IntModifier, res);
            
            if (ComplexAmount.ClampModifier == ClampModifier.Min)
                return Mathf.Max(ComplexAmount.IntModifier, res);

            return res;
        }

        return IntModifier;
    }
}
