using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ConditionType
{
    None,
    CardPlayCost,
    CardElement,
    CoinsAmount,
    CardType,
    Sanity,
    CardCount
}

public enum OrdinationModifier
{
    None,
    Equal,
    AboveOrEqual,
    Above,
    BelowOrEqual,
    Below
}

[CreateAssetMenu(menuName = "Condition")]
public class AbilityCondition : RichModifiers
{
    [Header("Condition")]
    [Space(30)]
    public ConditionType Type;
    public OrdinationModifier OrdinationModifier;

    public bool TestOrdination(int n)
    {
        if (OrdinationModifier == OrdinationModifier.Above)
            return n > GetAmount();

        if (OrdinationModifier == OrdinationModifier.Below)
            return n < GetAmount();

        if (OrdinationModifier == OrdinationModifier.Equal)
            return n == GetAmount();

        if (OrdinationModifier == OrdinationModifier.AboveOrEqual)
            return n >= GetAmount();

        if (OrdinationModifier == OrdinationModifier.BelowOrEqual)
            return n <= GetAmount();

        return false;
    }
}
