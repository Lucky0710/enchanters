using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using Coffee.UIExtensions;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class AnimationTemplate
{
    public AnimationTemplate(Action<AnimationTemplate> animFunc, 
        List<Card> card, float duration, float length, bool forLocal = true, 
        List<int> intModifiers = null, float floatModifier = 0f)
    {
        AnimFunc = animFunc;
        Cards = card;
        ForLocalPlayer = forLocal;
        IntModifiers = intModifiers;
        Duration = duration;
        Length = length;
        FloatModifier = floatModifier;
    }
    public Action<AnimationTemplate> AnimFunc;
    public List<Card> Cards;
    public bool ForLocalPlayer;
    public List<int> IntModifiers;
    public float Duration;
    public float Length;
    public float FloatModifier;
}

public class AnimationManager : MonoBehaviour
{
    public static AnimationManager Instance => _instance;
    private static AnimationManager _instance;

    private void Awake()
    {
        _instance = this;
    }

    private bool _inAnimation;
    private List<AnimationTemplate> _animationPile = new List<AnimationTemplate>();

    public Transform MarketStart;
    public Transform MarketEnd;
    public List<Transform> MarketSlots;

    public Transform LocalPlayerDrawStart;
    public Transform OpponentDrawStart;
    public Transform LocalPlayerHandRoot;
    public Transform OpponentHandRoot;
    public List<Transform> MulliganRoots;
    public List<Transform> ChosenCardsRoot;
    public Transform PlayedCardRoot;
    public Transform LocalDiscardPileRoot;
    public Transform OpponentDiscardPileRoot;
    public Transform OpponentLifePoints;
    public Transform LocalPlayerLifePoints;

    public Transform TopLeft;
    public Transform TopRight;
    public Transform BottomLeft;

    public Transform UIParticlesRoot;

    public UIParticle LightProcParticlesPrefab;
    public UIParticle WaterProcParticlesPrefab;
    public UIParticle DarkProcParticlesPrefab;

    public float MaxAnimHeight;
    public float MinAnimHeight;


    public void AppendAnim(AnimationTemplate newAnim)
    {
        _animationPile.Add(newAnim);
        if (!_inAnimation)
            PlayNextAnim();
    }

    public void RemoveAnim(AnimationTemplate anim)
    {
        _animationPile.Remove(anim);
    }
    
    private void PlayNextAnim()
    {
        _inAnimation = true;
        
        if (_animationPile.Count <= 0)
        {
            _inAnimation = false;
            MatchController.Instance.CheckForGameOver();
            return;
        }

        AnimationTemplate nextAnim = _animationPile[0];
        _animationPile.Remove(nextAnim);
        nextAnim.AnimFunc(nextAnim);
        DOVirtual.DelayedCall(nextAnim.Length, PlayNextAnim);
    }

    public void SlideToMarketSlot(AnimationTemplate anim)
    {
        anim.Cards[0].transform.DOMove(MarketSlots[anim.IntModifiers[0] ].transform.position, anim.Duration)
            .SetEase(Ease.OutQuad);
        if (anim.IntModifiers[0] == 4 || anim.IntModifiers[0] == 0)
        {
            anim.Cards[0].transform.DOScale(1f, anim.Duration);
        }
        else if (anim.IntModifiers[0] != 0)
        {
            anim.Cards[0].transform.DOScale(1.5f, anim.Duration);
        }
    }
    
    public void PopNewMarketCard(AnimationTemplate anim)
    {
        anim.Cards[0].gameObject.SetActive(true);
        //Inverted
        if (anim.ForLocalPlayer)
        {
            anim.Cards[0].transform.DOMove(MarketSlots[MarketSlots.Count - 1].transform.position, anim.Duration)
                .SetEase(Ease.OutQuad).From(MarketEnd.transform.position);
            anim.Cards[0].transform.DOScale(1f, anim.Duration).From(0f);
        }
        //Not inverted
        else
        {
            anim.Cards[0].transform.DOMove(MarketSlots[0].transform.position, anim.Duration)
                .SetEase(Ease.OutQuad).From(MarketStart.transform.position);
            anim.Cards[0].transform.DOScale(1f, anim.Duration).From(0f);
        }
    }
    
    public void DisapearLastMarketCard(AnimationTemplate anim)
    {
        //Inverted
        if (anim.ForLocalPlayer)
        {
            anim.Cards[0].transform.DOMove(MarketStart.transform.position, anim.Duration)
                .SetEase(Ease.OutQuad);
            anim.Cards[0].transform.DOScale(0f, anim.Duration).OnComplete(() =>
            {
                anim.Cards[0].gameObject.SetActive(false);
                anim.Cards[0].transform.localScale = Vector3.one;
            });
        }
        //Not inverted
        else
        {
            anim.Cards[0].transform.DOMove(MarketEnd.transform.position, anim.Duration)
                .SetEase(Ease.OutQuad);
            anim.Cards[0].transform.DOScale(0f, anim.Duration).OnComplete(() =>
            {
                anim.Cards[0].gameObject.SetActive(false);
                anim.Cards[0].transform.localScale = Vector3.one;
            });
        }
    }

    public void DrawCard(AnimationTemplate anim)
    {
        anim.Cards[0].transform.localScale = Vector3.one * UIControllerForGame.Instance.CardInHandSize;
        
        if (anim.ForLocalPlayer)
        {
            anim.Cards[0].transform.position = LocalPlayerDrawStart.position;
            Vector3 endPos = MatchController.Instance.LocalPlayer.ComputeCardPositionInHand(anim.Cards[0], true) +
                             LocalPlayerHandRoot.position;
            anim.Cards[0].transform.DOMove(endPos, anim.Duration).SetEase(Ease.OutQuad);
            float tilt = MatchController.Instance.LocalPlayer.ComputeCardTiltInHand(anim.Cards[0]);
            anim.Cards[0].transform.DORotate(new Vector3(0f, 0f, tilt), anim.Duration);
        }
        else
        {
            anim.Cards[0].transform.position = OpponentDrawStart.position;
            anim.Cards[0].transform.localEulerAngles = new Vector3(0f, 180f, 0f);
            Vector3 endPos = MatchController.Instance.Opponent.ComputeCardPositionInHand(anim.Cards[0], false) +
                             OpponentHandRoot.position;
            anim.Cards[0].transform.DOMove(endPos, anim.Duration).SetEase(Ease.OutQuad);

            float tilt = MatchController.Instance.Opponent.ComputeCardTiltInHand(anim.Cards[0]);
            anim.Cards[0].transform.DORotate(new Vector3(0f, 180f, tilt), anim.Duration);
        }
    }
    
    public void PopMulliganCard(AnimationTemplate anim)
    {
        anim.Cards[0].transform.position = OpponentDrawStart.position;
        Vector3 endPos = MulliganRoots[anim.IntModifiers[0]].position;
        anim.Cards[0].transform.DOMove(endPos, anim.Duration).SetEase(Ease.OutQuad);
    }
    
    public void ChooseMulliganCard(AnimationTemplate anim)
    {
        Vector3 endPos = ChosenCardsRoot[anim.IntModifiers[0]].position;
        anim.Cards[0].transform.DOMove(endPos, anim.Duration).SetEase(Ease.OutQuad);
        anim.Cards[0].BuyingOutline.SetActive(false);
    }

    public void DiscardCard(AnimationTemplate anim)
    {
        anim.Cards[0].transform.DOScale(1f, anim.Duration).SetEase(Ease.OutQuad);
        anim.Cards[0].transform.DORotate(new Vector3(0f, 0f, 0f), anim.Duration);
        
        if (anim.ForLocalPlayer)
        {
            anim.Cards[0].transform.DOMove(LocalDiscardPileRoot.transform.position 
                                           + new Vector3(0f, 0f, -anim.FloatModifier), anim.Duration).SetEase(Ease.OutQuad);
        }
        else
        {
            anim.Cards[0].transform.DOMove(OpponentDiscardPileRoot.transform.position 
                                           + new Vector3(0f, 0f, -anim.FloatModifier), anim.Duration).SetEase(Ease.OutQuad);
        }
    }

    public void ProcLightCard(AnimationTemplate anim)
    {
        SpellSlot slot = MatchController.Instance.GetSlotContainingCard(anim.Cards[0]);
        var particles = Instantiate(LightProcParticlesPrefab, UIParticlesRoot);
        particles.transform.position = Camera.main.WorldToScreenPoint(slot.transform.position);
        particles.Play();
        DOVirtual.DelayedCall(3f, () => Destroy(particles));
    }

    public void ProcWaterCard(AnimationTemplate anim)
    {
        SpellSlot slot = MatchController.Instance.GetSlotContainingCard(anim.Cards[0]);
        var particles = Instantiate(WaterProcParticlesPrefab, UIParticlesRoot);
        particles.transform.position = Camera.main.WorldToScreenPoint(slot.transform.position);
        particles.Play();
        DOVirtual.DelayedCall(3f, () => Destroy(particles));
    }
    
    public void ProcDarkCard(AnimationTemplate anim)
    {
        SpellSlot slot = MatchController.Instance.GetSlotContainingCard(anim.Cards[0]);
        var particles = Instantiate(DarkProcParticlesPrefab, UIParticlesRoot);
        particles.transform.position = Camera.main.WorldToScreenPoint(slot.transform.position);
        particles.Play();
        DOVirtual.DelayedCall(3f, () => Destroy(particles));
    }
    
    /*
    public void CastSmallFireballsFromCardInSlot(AnimationTemplate anim)
    {
        var slot = MatchController.Instance.GetSlotContainingCard(anim.Cards[0]);
        for (int i = 0; i < Mathf.Abs(anim.IntModifiers[0]); i++)
        {
            GameObject fireball = Instantiate(SmallFireBallPrefab, slot.transform.position, Quaternion.identity);
            TravelToPoint(anim.ForLocalPlayer ? OpponentLifePoints.position : LocalPlayerLifePoints.position, fireball.transform, anim.Duration, 1f);
            DOVirtual.DelayedCall(anim.Duration, () => Destroy(fireball));
        }

        DOVirtual.DelayedCall(anim.Duration, () =>
        {
            if (anim.ForLocalPlayer)
                UIControllerForGame.Instance.OpponentPlayerStats.FireImpactParticles.Play();
            else
                UIControllerForGame.Instance.LocalPlayerStats.FireImpactParticles.Play();
        });
    }
    
    public void CastBigFireballFromCardInSlot(AnimationTemplate anim)
    {
        var slot = MatchController.Instance.GetSlotContainingCard(anim.Cards[0]);
        GameObject fireball = Instantiate(FireBallPrefab, slot.transform.position, Quaternion.identity);
        TravelToPoint(anim.ForLocalPlayer ? OpponentLifePoints.position : LocalPlayerLifePoints.position, fireball.transform, anim.Duration, 1f);
        DOVirtual.DelayedCall(anim.Duration, () => Destroy(fireball));
        
        DOVirtual.DelayedCall(anim.Duration, () =>
        {
            if (anim.ForLocalPlayer)
                UIControllerForGame.Instance.OpponentPlayerStats.FireImpactParticles.Play();
            else
                UIControllerForGame.Instance.LocalPlayerStats.FireImpactParticles.Play();
        });
    }
    */
    
    public void TravelToPoint(Vector3 destination, Transform obj, float duration, float amplitude)
    {
        Vector3 StartPos = obj.position;
        Vector3 p2 = new Vector3(Random.Range(TopLeft.position.x, TopRight.position.x), Random.Range(TopLeft.position.y, BottomLeft.position.y));
        p2 *= amplitude;
        Vector3 p3 = new Vector3(Random.Range(TopLeft.position.x, TopRight.position.x), Random.Range(TopLeft.position.y, BottomLeft.position.y));
        p3 *= amplitude;
        if ((p2 - StartPos).magnitude >= (p3 - StartPos).magnitude)
        {
            Vector3 tmp = p2;
            p2 = p3;
            p3 = tmp;
        }

        DOVirtual.Float(0f, 1f, duration, value =>
        {
            var pos = CalculateCubicBezierPoint(value, StartPos, p2, p3, destination);
            pos.z = Mathf.Lerp(MinAnimHeight, MaxAnimHeight, 0.5f - Mathf.Abs(value - 0.5f));
            obj.transform.position = pos;
        }).SetEase(Ease.InQuad);
    }
    
    public Vector3 CalculateCubicBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;
        
        Vector3 p = uuu * p0; 
        p += 3 * uu * t * p1; 
        p += 3 * u * tt * p2; 
        p += ttt * p3; 
        
        return p;
    }
}
