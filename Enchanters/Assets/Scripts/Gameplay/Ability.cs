using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;
using UnityEngine.Serialization;

public enum AbilityName
{
    None,
    IncreaseLifePoints,
    IncreaseSanity,
    PlaceCardInHand,
    AddDamageToFireSpells,
    IncreaseMana,
    IncreaseCoins,
    SwapPlayCostWithBuyCost,
    EnableSpendMadnessInsteadOfMana,
    IncreaseCardPrice,
    StealCoins,
    EnableLoseAllCoinsAtTurnEnd,
    SetPlayCost,
    IncreasePlayCost,
    SendToDiscardPile,
    ResetSpellSlot = 16,
    ReduceDamages,
    IncrementMarket,
    SwapPlayCost,
    HideStats,
    WinTheGame,
    DisableSpell
}

public enum TriggerKeyword
{
    None,
    PlayedCard,
    HandIsEmpty,
    IncreasedMadness,
    TurnStart,
    BoughtCard,
    LearnedSpell
}

public enum PlayerTarget
{
    None,
    Opponent,
    LocalPlayer,
    Both
}

public enum CardTarget
{
    None,
    LastCardAddedToHand,
    LastPlayedCard,
    DiscardPile,
    TopDiscarded,
    Market,
    InPlaySpells,
    PlayedThisTurn,
    Hand,
    LastBoughtCard,
    LastCastSpell
}


[CreateAssetMenu(menuName = "Ability")]
public class Ability : RichModifiers
{
    [Header("Ability")]
    [Space(30)]
    public AbilityName AbilityName;
    public TriggerKeyword Trigger;
    public PlayerTarget TriggerPlayerTargetModifier;
    public PlayerTarget PlayerTarget;
    public AbilityCondition Condition;
    public int RepeatCounter = 1;

    public void SetValues(Ability a)
    {
        if (a == null)
            return;
        
        Condition = a.Condition;
        Trigger = a.Trigger;
        AbilityName = a.AbilityName;
        IntModifier = a.IntModifier;
        StringModifier = a.StringModifier;
        LinkedAbilities = a.LinkedAbilities;
        TriggerKeywordModifier = a.TriggerKeywordModifier;
        LinkedCards = a.LinkedCards;
        ComplexAmount = a.ComplexAmount;
        PlayerTargetModifier = a.PlayerTargetModifier;
        BoolModifier = a.BoolModifier;
        CardTargetFilters = a.CardTargetFilters;
        FloatModifier = a.FloatModifier;
        RepeatCounter = a.RepeatCounter;
        RandomFromTargets = a.RandomFromTargets;
    }
}
