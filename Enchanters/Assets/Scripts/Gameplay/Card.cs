using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun.UtilityScripts;
using TMPro;
using UnityEditor;
using UnityEngine;

public enum CardType
{
    None,
    Spell,
    Parchment,
    Passive
}

public enum Element
{
    None,
    Fire,
    Water,
    Air,
    Earth,
    Shadow,
    Light,
    Illusion
}

public class Card : MonoBehaviour
{
    public Guid Id;
    public string CodeName;
    public CardType Type;
    public Element Element;
    public int BuyCost;
    public int PlayCost;

    public MeshRenderer ArtworkRenderer;

    public TextMeshPro PlayCostText;
    public TextMeshPro BuyCostText;

    public Transform VisualRoot;
    
    public GameObject PlayableOutline;
    public GameObject PlayableOutlineHoovered;
    public GameObject BuyableOutline;
    public GameObject BuyableOutlineHoovered;
    public GameObject BuyingOutline;

    public GameObject SpellIndicator;
    public GameObject PassiveIndicator;
    public GameObject ParchmentIndicator;

    public bool CanBeHoovered = true;
    [HideInInspector] public bool SelectedInMulligan;

    public List<Ability> Abilities;
    
    private int _basePlayCost;
    private int _baseBuyCost;

    private void Start()
    {
        _baseBuyCost = BuyCost;
        _basePlayCost = PlayCost;
    }

    public void UpdateCostVisuals()
    {
        PlayCostText.text = PlayCost.ToString();
        BuyCostText.text = BuyCost.ToString();

        if (MatchController.Instance)
        {
            if (PlayCost < _basePlayCost)
                PlayCostText.color = Color.green;
            if (BuyCost < _baseBuyCost)
                BuyCostText.color = Color.green;
            
            if (PlayCost == _basePlayCost)
                PlayCostText.color = Color.white;
            if (BuyCost == _baseBuyCost)
                BuyCostText.color = Color.white;
            
            if (PlayCost > _basePlayCost)
                PlayCostText.color = Color.red;
            if (BuyCost > _baseBuyCost)
                BuyCostText.color = Color.red;
        }
    }

    public void UpdateIndicatorVisuals()
    {
        SpellIndicator.SetActive(Type == CardType.Spell);
        PassiveIndicator.SetActive(Type == CardType.Passive);
        ParchmentIndicator.SetActive(Type == CardType.Parchment);
    }

    public bool HasAbility(AbilityName name)
    {
        foreach (var ab in Abilities)
        {
            if (ab.AbilityName == name)
                return true;
        }

        return false;
    }

    public void ResetOutlines()
    {
        PlayableOutline.SetActive(false);
        PlayableOutlineHoovered.SetActive(false);
        BuyableOutline.SetActive(false);
        BuyableOutlineHoovered.SetActive(false);
        BuyingOutline.SetActive(false);
    }
}
