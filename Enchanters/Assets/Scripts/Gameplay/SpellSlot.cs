using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class SpellSlot : MonoBehaviour
{
    public MeshRenderer ArtworkHolder;
    public Card Card;
    public GameObject UsedMask;
    public GameObject UsableOutline;
    public GameObject UsableOutlineHoovered;
    public GameObject DeleteOutlineHoovered;
    public GameObject DeleteSpellButton;
    public Material BaseArtworkMaterial;
    
    private bool _used;

    public void Init(Card card, bool ownedByLocal)
    {
        ArtworkHolder.material = card.ArtworkRenderer.material;
        Card = Instantiate(card, MatchController.Instance.CleanRoot);
        Card.ResetOutlines();
        Card.gameObject.SetActive(false);
        DeleteSpellButton.SetActive(ownedByLocal);
        Reset();
    }

    public void Free()
    {
        Card = null;
        ArtworkHolder.material = BaseArtworkMaterial;
        DeleteSpellButton.SetActive(false);
    }

    public bool IsUsed()
    {
        return _used;
    }

    public void Use()
    {
        if (Card.Type == CardType.Passive)
            return;
        
        _used = true;
        UsableOutline.SetActive(false);
        UsedMask.SetActive(true);
    }

    public void Reset()
    {
        _used = false;
        UsedMask.SetActive(false);
    }

    public void UpdateOutline(bool usable)
    {
        UsableOutline.SetActive(Card && Card.Type != CardType.Passive && usable && !_used);
    }
}
