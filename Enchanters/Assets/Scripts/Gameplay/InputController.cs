using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Networking.Match;
using Debug = UnityEngine.Debug;

public class InputController : MonoBehaviour
{
    public static InputController Instance => _instance;
    private static InputController _instance;

    private void Awake()
    {
        _instance = this;
    }

    private Card _hooveredCard;
    private Card _holdedCard;

    public float SmoothMoveSpeedLimitForHand;
    public float SmoothMoveSpeedLimitForMarket;
    public float PlayingLimitRatio;
    public float BuyingLimitRatio;

    public float ThresholdToShowCardSlotDetail = 0.1f;
    private SpellSlot _hooveredSlot;
    private SpellSlot _deleteButtonHoovered;
    private Tween _showSlotDetailsTween;

    private float _previousYpos;

    private void Update()
    {
        bool hooveringCard = false;
        bool hooveringSlot = false;
        bool hooveredDeleteButton = false;
        
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit[] hits = Physics.RaycastAll(ray);
        foreach (var hit in hits)
        {
            //Card Hoovering
            if (hit.collider.CompareTag("Card") && !hooveringCard && !_holdedCard)
            {
                hooveringCard = true;
                
                Card hooveredCard = hit.collider.GetComponent<Card>();
                if (hooveredCard != _hooveredCard && hooveredCard.CanBeHoovered)
                {
                    //Hand
                    if (_hooveredCard != null && MatchController.Instance.IsInLocalPlayerHand(_hooveredCard))
                        UIControllerForGame.Instance.UnhooverCardInHand(_hooveredCard);

                    if (MatchController.Instance.IsInLocalPlayerHand(hooveredCard))
                    {
                        if (_hooveredCard != null)
                            UIControllerForGame.Instance.HooverCardInHand(hooveredCard, false);
                        else
                            UIControllerForGame.Instance.HooverCardInHand(hooveredCard, true);

                        _hooveredCard = hooveredCard;
                    }
                    
                    //Market
                    if (_hooveredCard != null && MatchController.Instance.IsCardInMarket(_hooveredCard))
                        UIControllerForGame.Instance.UnhooverCardInMarket(_hooveredCard);

                    if (MatchController.Instance.IsCardInMarket(hooveredCard) 
                        && MatchController.Instance.Market.IndexOf(hooveredCard) != 0
                        && MatchController.Instance.Market.IndexOf(hooveredCard) != 4
                        && MatchController.Instance.IsLocalPlayerTurn())
                    {
                        _hooveredCard = hooveredCard;
                        UIControllerForGame.Instance.HooverCardInMarket(hooveredCard);
                    }
                    
                    //Mulligan
                    if (_hooveredCard != null && MatchController.Instance.IsMulligan() && MatchController.Instance.IsCardInMulligan(_hooveredCard))
                        UIControllerForGame.Instance.UnhooverCardInMulligan(_hooveredCard);

                    if (MatchController.Instance.IsMulligan() && MatchController.Instance.IsCardInMulligan(hooveredCard) 
                                                              && !MatchController.Instance.SelectedEnoughCardsInMulligan())
                    {
                        _hooveredCard = hooveredCard;
                        UIControllerForGame.Instance.HooverCardInMulligan(hooveredCard);
                    }
                }
            }

            //Card Holding
            if (hit.collider.CompareTag("CardVisualRoot"))
            {
                Card holdedCard = hit.collider.GetComponentInParent<Card>();
                if (Input.GetMouseButtonDown(0) && holdedCard && !_holdedCard)
                {
                    if (MatchController.Instance.IsInLocalPlayerHand(holdedCard))
                    {
                        UIControllerForGame.Instance.HoldCard(holdedCard);
                        _holdedCard = holdedCard;
                    }
                    else if (MatchController.Instance.IsCardInMarket(holdedCard) 
                             && MatchController.Instance.CanBuyCard(MatchController.Instance.LocalPlayer, holdedCard)
                             && MatchController.Instance.Market.IndexOf(holdedCard) != 0
                             && MatchController.Instance.Market.IndexOf(holdedCard) != 4
                             && MatchController.Instance.IsLocalPlayerTurn())
                    {
                        _holdedCard = holdedCard;
                        _holdedCard.VisualRoot.localPosition += new Vector3(0f, 0f, -0.1f);
                    }
                }
            }
            
            //Clicking on Mulligan Card
            if (hit.collider.CompareTag("CardVisualRoot"))
            {
                Card clickedCard = hit.collider.GetComponentInParent<Card>();
                if (Input.GetMouseButtonDown(0))
                {
                    if (MatchController.Instance.IsCardInMulligan(clickedCard))
                    {
                        if (clickedCard.SelectedInMulligan || !MatchController.Instance.SelectedEnoughCardsInMulligan())
                        {
                            clickedCard.SelectedInMulligan = !clickedCard.SelectedInMulligan;
                        }

                        clickedCard.BuyingOutline.SetActive(clickedCard.SelectedInMulligan);
                        UIControllerForGame.Instance.UpdateUI();
                    }
                }
            }
            
            //Clicking on Delete spell btn
            if (hit.collider.CompareTag("SlotDeleteSpell"))
            {
                hooveredDeleteButton = true;
                
                SpellSlot slot = hit.collider.GetComponentInParent<SpellSlot>();
                if (slot && !_holdedCard && MatchController.Instance.IsSlotOwnedByLocalPlayer(slot))
                {
                    _deleteButtonHoovered = slot;
                    _deleteButtonHoovered.DeleteOutlineHoovered.SetActive(true);
                    if (Input.GetMouseButtonDown(0) && MatchController.Instance.TryFreeSlot(slot))
                    {
                        UIControllerForGame.Instance.HideSpellSlotDetails();
                        break;
                    }
                }
            }
            
            //Spell slot hoovering and clicked
            if (hit.collider.CompareTag("SpellSlot"))
            {
                hooveringSlot = true;
                SpellSlot slot = hit.collider.GetComponentInParent<SpellSlot>();
                if (slot && !_holdedCard && slot.Card)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        if (!slot.IsUsed() && MatchController.Instance.IsSlotOwnedByLocalPlayer(slot) && MatchController.Instance.TryCastSpell(slot))
                        {
                            if (_hooveredCard)
                                _hooveredSlot.UsableOutlineHoovered.SetActive(false);
                        }
                    }
                    else if (!_hooveredSlot || _hooveredSlot != slot)
                    {
                        if (_hooveredSlot)
                            _hooveredSlot.UsableOutlineHoovered.SetActive(false);
                        
                        _showSlotDetailsTween.Kill();
                        _hooveredSlot = slot;
                        if (MatchController.Instance.IsSlotOwnedByLocalPlayer(slot))
                            _hooveredSlot.UsableOutlineHoovered.SetActive(true);
                        
                        _showSlotDetailsTween = DOVirtual.DelayedCall(ThresholdToShowCardSlotDetail, () =>
                        {
                            UIControllerForGame.Instance.ShowSpellSlotDetails(slot);
                        });
                    }
                }
            }
        }
        
        //Slot Unhoovering
        if (!hooveringSlot && _hooveredSlot)
        {
            _hooveredSlot.UsableOutlineHoovered.SetActive(false);
            _hooveredSlot = null;
            _showSlotDetailsTween.Kill();
            UIControllerForGame.Instance.HideSpellSlotDetails();
        }
        
        //Slot delete button Unhoovering
        if (!hooveredDeleteButton && _deleteButtonHoovered)
        {
            _deleteButtonHoovered.DeleteOutlineHoovered.SetActive(false);
            _deleteButtonHoovered = null;
        }

        //Card Unhoovering
        if (!hooveringCard && _hooveredCard)
        {
            if (_holdedCard != _hooveredCard && MatchController.Instance.IsInLocalPlayerHand(_hooveredCard))
                UIControllerForGame.Instance.UnhooverCardInHand(_hooveredCard);
            
            if (_holdedCard != _hooveredCard && MatchController.Instance.IsCardInMarket(_hooveredCard))
                UIControllerForGame.Instance.UnhooverCardInMarket(_hooveredCard);
            
            if (_holdedCard != _hooveredCard && MatchController.Instance.IsCardInMulligan(_hooveredCard))
                UIControllerForGame.Instance.UnhooverCardInMulligan(_hooveredCard);
            
            _hooveredCard = null;
        }

        //Card Releasing
        if (Input.GetMouseButtonUp(0) && _holdedCard)
        {
            float yPos = Input.mousePosition.y;
            float ratio = yPos / Screen.height;
            if (MatchController.Instance.LocalPlayer.Hand.Contains(_holdedCard))
            {
                if (ratio >= PlayingLimitRatio && MatchController.Instance.TryPlayCard(_holdedCard))
                {
                    _holdedCard.PlayableOutline.SetActive(false);
                    _holdedCard.PlayableOutlineHoovered.SetActive(false);
                    _holdedCard = null;
                }
                else
                {
                    UIControllerForGame.Instance.PlaceCardInHand(true, _holdedCard);
                    _holdedCard.PlayableOutlineHoovered.SetActive(false);
                    _holdedCard.CanBeHoovered = false;
                    Card toReset = _holdedCard;
                    DOVirtual.DelayedCall(0.3f, () => toReset.CanBeHoovered = true);
                    _holdedCard = null;
                }
            }
            else if (MatchController.Instance.IsCardInMarket(_holdedCard))
            {
                if (ratio <= BuyingLimitRatio && MatchController.Instance.Market.IndexOf(_holdedCard) != 0 
                                              && MatchController.Instance.Market.IndexOf(_holdedCard) != 4 
                                              && MatchController.Instance.TryBuyCard(_holdedCard))
                {
                    _holdedCard.BuyableOutline.SetActive(false);
                    _holdedCard.BuyableOutlineHoovered.SetActive(false);
                    _holdedCard.CanBeHoovered = false;
                    Card toReset = _holdedCard;
                    DOVirtual.DelayedCall(0.3f, () => toReset.CanBeHoovered = true);
                    _holdedCard = null;
                }
                else
                {
                    UIControllerForGame.Instance.ReplaceCardInMarketAtRelease(_holdedCard);
                    _holdedCard.BuyableOutlineHoovered.SetActive(false);
                    _holdedCard.CanBeHoovered = false;
                    Card toReset = _holdedCard;
                    DOVirtual.DelayedCall(0.3f, () => toReset.CanBeHoovered = true);
                    _holdedCard = null;
                }
            }
        }

        //Card Dragging
        if (_holdedCard)
        {
            if (MatchController.Instance.IsCardInMarket(_holdedCard))
            {
                Vector3 ray2 = Input.mousePosition;
                ray2.z = Camera.main.WorldToScreenPoint(_holdedCard.transform.position).z;
                Vector3 pos = Camera.main.ScreenToWorldPoint(ray2);
                pos.z = _holdedCard.transform.position.z;
            
                Vector3 dir = pos - _holdedCard.transform.position;
                if (dir.magnitude >= SmoothMoveSpeedLimitForMarket)
                    _holdedCard.transform.position += dir * SmoothMoveSpeedLimitForMarket;
            }
            else if (MatchController.Instance.LocalPlayer.Hand.Contains(_holdedCard))
            {
                Vector3 ray2 = Input.mousePosition;
                ray2.z = Camera.main.WorldToScreenPoint(_holdedCard.transform.position).z;
                Vector3 pos = Camera.main.ScreenToWorldPoint(ray2);
                pos.z = _holdedCard.transform.position.z;
            
                Vector3 dir = pos - _holdedCard.transform.position;
                if (dir.magnitude >= SmoothMoveSpeedLimitForHand)
                    _holdedCard.transform.position += dir * SmoothMoveSpeedLimitForHand;
            }
            
        }

        //Card outline emphasize when dragging
        if (_holdedCard && MatchController.Instance.IsLocalPlayerTurn())
        {
            float yPos = Input.mousePosition.y;
            float ratio = yPos / Screen.height;
            if (MatchController.Instance.LocalPlayer.Hand.Contains(_holdedCard))
            {
                if (_previousYpos < PlayingLimitRatio && ratio >= PlayingLimitRatio)
                    UIControllerForGame.Instance.EmphasizeOutlineForCard(_holdedCard.PlayableOutline);
            }
            else if (MatchController.Instance.IsCardInMarket(_holdedCard) 
                     && MatchController.Instance.CanBuyCard(MatchController.Instance.LocalPlayer, _holdedCard)
                     && MatchController.Instance.Market.IndexOf(_holdedCard) != 0
                     && MatchController.Instance.Market.IndexOf(_holdedCard) != 4)
            {
                if (_previousYpos > BuyingLimitRatio && ratio <= BuyingLimitRatio)
                {
                    _holdedCard.BuyableOutline.SetActive(false);
                    _holdedCard.BuyingOutline.SetActive(true);
                    UIControllerForGame.Instance.EmphasizeOutlineForCard(_holdedCard.BuyingOutline);
                }
                else if (_previousYpos <= BuyingLimitRatio && ratio > BuyingLimitRatio)
                {
                    _holdedCard.BuyableOutline.SetActive(true);
                    _holdedCard.BuyingOutline.SetActive(false);
                }
            }
            
            _previousYpos = ratio;
        }
    }
}
