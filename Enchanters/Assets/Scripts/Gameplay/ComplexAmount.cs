using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ComplexAmountType
{
    None,
    BuyCost,
    PlayCost,
    CardCount,
    Coins
}

public enum Operation
{
    None,
    Add,
    Mult
}

public enum ClampModifier
{
    None,
    Min,
    Max
}

[CreateAssetMenu(menuName = "Complex Amount")]
public class ComplexAmount : RichModifiers
{
    [Header("Complex Amount")]
    [Space(30)]
    public ComplexAmountType Type;
    public Operation OperationWithNestedCA;
    public ClampModifier ClampModifier;
}
