using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UsernamePopup : Popup
{
    public InputField UsernameInputField;

    public override void Open()
    {
        base.Open();
        UsernameInputField.text = "";
    }

    public void OnUsernameConfirmed()
    {
        MenuController.Instance.CreateAccount(UsernameInputField.text);
        Close();
        UIControllerForMenu.Instance.MainMenuScreen.AccountsPopup.Close();
    }

    public void OnUsernameCanceled()
    {
        Close();
    }
}
