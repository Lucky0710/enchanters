using System.Collections;
using System.Collections.Generic;
using Coffee.UIExtensions;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStatsUI : MonoBehaviour
{
    public Text LifePointsText;
    public Text ManaText;
    public Text CoinsText;
    public Text WisdomText;
    public Text MadnessText;

    public Image LifePointsBG;
    public Image ManaBG;
    public Image CoinsBG;
    public Image OverGradient;

    public Color LifePointsBaseColor;
    public Color LifePointsHealedColor;
    public Color MadnessColor;
    public Color WisdomColor;

    public UIParticle ManaIncreaseParticles;
    public UIParticle CoinsIncreaseParticles;
    public UIParticle LifePointsLostParticles;
    public UIParticle FireImpactParticles;
    public UIParticle HealingParticles;

    public Transform CursorRoot;
    public GameObject Root;

    private PlayerController _playerController;

    public void InitPlayer(PlayerController player)
    {
        _playerController = player;
        InitBaseValues();
    }

    public void UpdateDisplay(bool show)
    {
        Root.SetActive(show);
    }

    public void InitBaseValues()
    {
        UpdateMana(0, MatchController.Instance.BaseManaGrowth, false);
        UpdateCoins(0, MatchController.Instance.BaseCoinsGrowth, false);
        LifePointsText.text = MatchController.Instance.BaseLifePoints.ToString();
        LifePointsBG.fillAmount = 1f;
        WisdomText.text = "0";
        MadnessText.text = "0";
        CursorRoot.transform.eulerAngles = Vector3.zero;
    }

    public void UpdateLifePoints(int lp, bool added, Element damageType)
    {
        LifePointsText.text = lp.ToString();
        LifePointsBG.DOFillAmount((float) lp / MatchController.Instance.BaseLifePoints, 0.5f).SetEase(Ease.OutQuad);

        if (added)
        {
            LifePointsText.DOColor(Color.white, 2f).From(Color.yellow).SetEase(Ease.InQuad);
            LifePointsBG.color = LifePointsHealedColor;
            LifePointsBG.DOColor(LifePointsBaseColor, 4f);
            HealingParticles.Play();
        }
        else
        {
            LifePointsText.DOColor(Color.white, 2f).From(Color.red).SetEase(Ease.InQuad);
            if (damageType == Element.Fire)
                FireImpactParticles.Play();
            else
            {
                LifePointsLostParticles.Play();
            }
        }
    }
    
    public void UpdateMana(int mana, int manaGrowth, bool added)
    {
        ManaText.text = mana + "/" + manaGrowth;
        if (manaGrowth != 0)
            ManaBG.DOFillAmount((float) mana / manaGrowth, 0.5f).SetEase(Ease.OutQuad);

        if (added)
        {
            ManaText.DOColor(Color.white, 2f).From(Color.cyan).SetEase(Ease.InQuad);
            ManaIncreaseParticles.Play();
        }
    }
    
    public void UpdateCoins(int coins, int coinGrowth, bool added)
    {
        CoinsText.text = coins + "/" + coinGrowth;
        if (coinGrowth != 0)
            CoinsBG.DOFillAmount((float) coins / coinGrowth, 0.5f).SetEase(Ease.OutQuad);

        if (added)
        {
            CoinsText.DOColor(Color.white, 2f).From(Color.yellow).SetEase(Ease.InQuad);
            CoinsIncreaseParticles.Play();
        }
    }

    public void UpdateSanity(int sanity, bool added)
    {
        UpdateWisdom(sanity);
        UpdateMadness(sanity);
        float zRot = Mathf.Lerp(-90, 90, sanity / 60f + 0.5f);
        CursorRoot.transform.DOKill();
        CursorRoot.transform.DOLocalRotate(new Vector3(0f, 0f, zRot), 0.5f).SetEase(Ease.OutQuad);
        
        if (added)
            OverGradient.color = WisdomColor;
        else
            OverGradient.color = MadnessColor;

        OverGradient.DOFade(0f, 2f).SetEase(Ease.InQuad);
    }
    
    public void UpdateWisdom(int sanity)
    {
        if (sanity > 0)
            WisdomText.text = sanity.ToString();
        else
        {
            WisdomText.text = "0";
        }
    }
    
    public void UpdateMadness(int sanity)
    {
        if (sanity < 0)
            MadnessText.text = (-sanity).ToString();
        else
        {
            MadnessText.text = "0";
        }
    }
}
