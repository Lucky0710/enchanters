using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class BluredBG : MonoBehaviour
{
    public Image BG;
    private static readonly int Size = Shader.PropertyToID("_Size");
    public bool IsDeckView;

    public void Subscribe()
    {
        foreach (var bg in UIControllerForMenu.Instance.BluredBgs)
        {
            if (bg.IsDeckView)
            {
                UIControllerForMenu.Instance.BluredBgs.Add(this);
                return;
            }
            
            bg.BG.enabled = false;
        }

        UIControllerForMenu.Instance.BluredBgs.Add(this);
        BG.enabled = true;
        
    }

    private Tween _fadeInTween;
    public void FadeIn(float duration)
    {
        Subscribe();
        
        if (UIControllerForMenu.Instance.BluredBgs.Count == 1)
        {
            _fadeOutTween.Kill();
            _fadeInTween = DOVirtual.Float(0f, 2f, duration, value =>
            {
                BG.material.SetFloat(Size, value);
            });
        }
    }

    public void UnSubscribe()
    {
        BG.enabled = false;
        UIControllerForMenu.Instance.BluredBgs.Remove(this);
        if (UIControllerForMenu.Instance.BluredBgs.Count > 0)
        {
            UIControllerForMenu.Instance.BluredBgs[UIControllerForMenu.Instance.BluredBgs.Count - 1].BG.enabled = true;
            DOVirtual.Float(0f, 2f, 0.5f, value =>
            {
                if (UIControllerForMenu.Instance.BluredBgs.Count > 0)
                    UIControllerForMenu.Instance.BluredBgs[UIControllerForMenu.Instance.BluredBgs.Count - 1].BG.material.SetFloat(Size, value);
            });
        }
    }

    private Tween _fadeOutTween;
    public void FadeOut(float duration)
    {
        _fadeInTween?.Kill();
        _fadeOutTween = DOVirtual.Float(2f, 0f, duration, value =>
        {
            BG.material.SetFloat(Size, value);
        }).OnComplete(UnSubscribe);
    }
}
