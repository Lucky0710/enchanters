using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AccountSlot : MonoBehaviour
{
    public Text Pseudo;
    public Text EmptyText;
    public GameObject DeleteButton;

    private bool _existing;
    private User _user;

    public void Init(User user)
    {
        _existing = user != null;
        Pseudo.gameObject.SetActive(_existing);
        EmptyText.gameObject.SetActive(!_existing);
        DeleteButton.SetActive(_existing);

        if (user != null)
        {
            _user = user;
            Pseudo.text = user.Username;
        }
    }

    public void OnAccountClicked()
    {
        if (_existing)
        {
            MenuController.Instance.SwitchAccount(_user);
            UIControllerForMenu.Instance.MainMenuScreen.AccountsPopup.Close();
        }
        else
        {
            UIControllerForMenu.Instance.MainMenuScreen.UsernamePopup.Open();
        }
    }

    public void OnDeleteClicked()
    {
        UIControllerForMenu.Instance.MainMenuScreen.ConfirmDeleteAccountPopup.Init(_user);
    }
}
