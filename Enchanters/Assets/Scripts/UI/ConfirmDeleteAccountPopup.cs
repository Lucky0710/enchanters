using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfirmDeleteAccountPopup : Popup
{
    private User _userToDelete;
    
    public void Init(User userToDelete)
    {
        _userToDelete = userToDelete;
        Open();
    }

    public void OnDeleteAccountConfirmed()
    {
        MenuController.Instance.DeleteAccount(_userToDelete);
        Close();
    }
    
    public void OnDeleteAccountCanceled()
    {
        Close();
    }
}
