using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuScreen : MonoBehaviour
{
    public Text Username;
    public AccountsPopup AccountsPopup;
    public UsernamePopup UsernamePopup;
    public ConfirmDeleteAccountPopup ConfirmDeleteAccountPopup;
    
    public void OnAccountsClicked()
    {
        AccountsPopup.Open();
        Username.text = "";
    }

    public void OnQuitClicked()
    {
        Application.Quit();
    }
}
