using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class AccountsPopup : Popup
{
    public List<AccountSlot> Slots;

    public override void Open()
    {
        base.Open();
        Init();
    }

    public void Init()
    {
        for (int i = 0; i < 3; i++)
        {
            if (MenuController.Instance.LocalSave.Users.Count <= i)
                Slots[i].Init(null);
            else
                Slots[i].Init(MenuController.Instance.LocalSave.Users[i]);
        }
    }
}
