using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Popup : Fadable
{
    public GameObject Root;
    public GameObject NotFadableRoot;
    public GameObject HideButton;
    public bool Hiden = false;
    public Image DarkBG;
    public BluredBG BlurBG;
    private static readonly int Size = Shader.PropertyToID("_Size");
    public float AppearingDuration = 0.3f;
    public float DisapearingDuration = 0.2f;

    public virtual void Open()
    {
        Root.SetActive(true);
        if (NotFadableRoot)
            NotFadableRoot.SetActive(true);
        
        if (HideButton)
            HideButton.SetActive(true);

        Hiden = false;

        if (DarkBG)
        {
            DarkBG.gameObject.SetActive(true);
            DarkBG.DOFade(0.9f, AppearingDuration * 1.5f).From(0f);
        }

        if (BlurBG)
            BlurBG.FadeIn(AppearingDuration * 1.5f);
        
        Fade(true, AppearingDuration);
    }

    public virtual void Close()
    {
        if (NotFadableRoot)
            NotFadableRoot.SetActive(false);
        
        if (DarkBG)
        {
            DarkBG.DOFade(0f, DisapearingDuration).OnComplete(() =>
            {
                DarkBG.gameObject.SetActive(false);
            }); 
        }
        
        if (HideButton)
            HideButton.SetActive(false);

        if (BlurBG)
        {
            BlurBG.FadeOut(DisapearingDuration);
        }

        Fade(false, DisapearingDuration);

        DOVirtual.DelayedCall(DisapearingDuration, () =>
        {
            Root.SetActive(false);
        });
        
    }

    public void Hide()
    {
        Hiden = true;
        DarkBG.DOKill();
        DarkBG.DOFade(0f, DisapearingDuration).OnComplete(() =>
        {
            DarkBG.gameObject.SetActive(false);
        });
        
        Root.SetActive(false);
        if (NotFadableRoot)
            NotFadableRoot.SetActive(false);
        
        if (BlurBG)
            BlurBG.FadeOut(DisapearingDuration);
    }

    public void Show()
    {
        Root.SetActive(true);
        if (NotFadableRoot)
            NotFadableRoot.SetActive(true);
        
        if (HideButton)
            HideButton.SetActive(true);
        Hiden = false;
        DarkBG.gameObject.SetActive(true);
        DarkBG.DOKill();
        DarkBG.DOFade(0.8f, AppearingDuration * 1.5f).From(0f);

        if (BlurBG)
            BlurBG.FadeIn(AppearingDuration * 1.5f);
    }

    public virtual void OnHideBButtonClicked()
    {
        if (Hiden)
        {
            Show();
        }
        else
        {
            Hide();
        }
    }
}
