using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Fadable : MonoBehaviour
{
    public List<Image> FadingImages;
    public List<Text> FadingTexts;
    public List<CanvasGroup> CanvasGroups;
    public List<Fadable> FadableChildrens;
    public float MaxFade = 1f;
    
    public void Fade(bool _in, float duration)
    {
        foreach (var img in FadingImages)
        {
            img.DOFade(_in ? MaxFade : 0f, duration);
        }
        foreach (var txt in FadingTexts)
        {
            txt.DOFade(_in ? MaxFade : 0f, duration);
        }

        foreach (var child in FadableChildrens)
        {
            child.Fade(_in, duration);
        }
        
        foreach (var child in CanvasGroups)
        {
            child.DOFade(_in ? MaxFade : 0f, duration);
        }
    }
}
